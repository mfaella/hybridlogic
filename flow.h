#ifndef _FLOW_H_
#define _FLOW_H_

#include "basics.h"

class Flow {
private:
  Poly _pre, _post;
public:
  Flow(Poly flow):_pre(flow), _post(flow) {
    prepoly_assign(_pre);
  }
  Poly pre() const { return _pre; }
  Poly post() const { return _post; }
};

#endif
  
