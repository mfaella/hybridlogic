#ifndef _NYCS_PAIRWISE_REDUCE_
#define _NYCS_PAIRWISE_REDUCE_

#include <ppl.hh>

using namespace Parma_Polyhedra_Library;
typedef NNC_Polyhedron Poly;
typedef Pointset_Powerset<Poly> PS;

// Reference implementations
void fast_pairwise_reduce(PS &poly);
void fast_difference_assign(PS &x, PS &y);

void raw_pairwise_reduce(PS &poly);

// Utility functions to compare different implementations
void meta_pairwise_reduce(PS, bool check_correctness=false);
void meta_difference_assign(PS &x, PS &y, bool check_correctness=false);

/* Merges two reduced powersets into a single reduced powerset.
   a and b are supposed to be already reduced.
   dest is an output parameter (its content is destroyed).
*/
void merge_and_pairwise_reduce(PS &dest, PS &a, PS &b);

Poly simplify_coefficients(const Poly &p);


#endif
