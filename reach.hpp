
#include <ppl.hh>
#include <vector>
#include "basics.h"
#include "rwa_maps.h"

using namespace std;

typedef vector<pair<Poly, Poly>> ReachPairs;

ReachPairs reach0(const PPS & A, const Poly & B, const Poly & preflow);

ReachPairs reach_plus(const PPS & A, const Poly & B, const Poly & preflow);
