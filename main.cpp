#include "basics.h"
#include "loop.h"
#include "reach.hpp"
#include "ltlf_mc.hpp"

#include "automata/test1.h"
#include "automata/GandF.h"
#include "automata/phi1.h"
#include "automata/phi1-minimal.h"

using namespace std;

void example1();
void example2();
void example3();
void example4();
void example5();
void example6();
void example7();
void example8();
void example9();
void example_LTLf_Phi1();
void example2_LTLf_Phi1();

int main(void) {

//   Variable x(0), y(1), z(2);
//
//   Poly p1 = make_poly({ x > 0, x < 1, y > 0, y < 1 }),
//        p2 = make_poly({ x >= 1, x < 2, 2*y > 1, y < 1 }),
//     f  = make_poly({ y == 1, x >= -1, x <= 1 });
//
//   Loop loop { p1, p2 };
//   Flow flow { f };
//   cout << loop << endl;
//   cout << "Is proper loop: " << is_proper_loop(loop) << endl;
//   Loop l2 = pre_loop(loop, flow);
//   cout << l2 << endl;
//   Loop l3 = pre_loop(l2, flow);
//   cout << l3 << endl;
//
//   Poly q1 = traverse_loop(loop, flow);
//   cout << "Traverse: " << q1 << endl;

//   example1();
//   example2();
//   example3();
//   example4();
// example5();
// example7();
// example8();
// example9();
  example_LTLf_Phi1();
  // example2_LTLf_Phi1();
}

Loop computeLoop(Flow flow, Loop loop, int maxiter) {
  bool terminated = false;
  Loop next;
  for (int i=0; i<maxiter && !terminated; i++) {
    cout << "Iteration " << i << ":" << endl;
    (cout << "Current loop: " <<= loop) << endl;
    next = n_steps(loop, flow);
    terminated = (next == loop);
    cout << "Equal: " << terminated << endl;
    loop = next;
  }
  return loop;
}

/* A 3D example where all points reach a central point R.
   It converges after 1 iteration because all points are good. */
void example1() {

  cout << "**** Example 1 ********************************************" << endl;

  Variable x(0), y(1), z(2);

  Poly flow  = make_poly({
    point( x + y + 2*z ),
    point( 2*y + z ),
    point( x - y + z ),
    point( -2*x - 2*z ),
    point( -x + y ),
    point( x ),
    point( -y ),
    point( -x - y )
  });

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

  Poly p1 = make_poly({
    closure_point(),
    closure_point( y ),
    point( x + y )
  }, 3);
  Poly p2 = make_poly({
    closure_point(),
    closure_point( x + y ),
    point( x - y )
  }, 3);
  Poly p3 = make_poly({
    closure_point(),
    closure_point( x - y ),
    point( -x - y - 2*z )
  });
  Poly p4 = make_poly({
    closure_point(),
    closure_point( -x - y - 2*z ),
    point( -x + y - z )
  });
  Poly p5 = make_poly({
    closure_point(),
    closure_point( -x + y - z ),
    point( y ),
  });

  Loop loop({ p1, p2, p3, p4, p5 });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  Poly q1 = traverse_loop(loop, flow);
  cout << "Traverse: " << q1 << endl;
  cout << "Equality test: " << (q1 == p1) << endl;

  cout << "***********************************************************" << endl;

}

/** 2D example of two diagonal segments p and q, connecting at the origin.

    The algorithm does not converge,
    because no point can traverse the loop forever,
    but for all k there are points that can traverse it k times.
*/
void example2() {

  cout << "**** Example 2 ********************************************" << endl;

  Variable x(0), y(1);

  Poly flow  = make_poly({ x>=-5, x<=5, y==-1});

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

  Poly p = make_poly({ point(), point(-10*x -10*y) });
  Poly q = make_poly({ point(), point( 10*x -10*y) });
  Poly r = make_poly({ closure_point(), point( -y), ray(-x-y), ray(x-y) });

  Loop loop({ p, r, q, r });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first 5 iterations of the algorithms
  computeLoop(flow, loop, 10);
  cout << "***********************************************************" << endl;

}

/**
 *  Loop percorribile in tempo finito (Comportamento Zeno)
*/
void example3() {

  cout << "**** Example 3 ********************************************" << endl;

  Variable x(0), y(1), z(2);

  Poly flow  = make_poly({ x>=1, y>=-1, y<=1, z>=-1, z<=1 });

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

  Poly a = make_poly({
    closure_point(x + y), closure_point(-x + y), point(-x - y), point(x - y),
    closure_point(3*y + 4*z),
  });
  Poly b = make_poly({
    closure_point(x + y), closure_point(-x + y),
    point(x + y - 3*z), point(-x + y - 3*z),
    closure_point(x - y), closure_point(-x - y),
  });

  cout << "Poly a: " << a << endl;
  cout << "Poly b: " << b << endl;

  cout << "Generatori a: " << a.minimized_generators() << endl;
  cout << "Generatori b: " << b.minimized_generators() << endl;

  Poly ab(a);
  ab.intersection_assign(b);
  cout << "Intersection a b: " << ab.minimized_generators() << endl;

  cout << "Boundary a - b: " << bndry(a, b) << endl;

  Loop loop({ a, b });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first 10 iterations of the algorithms
  computeLoop(flow, loop, 10);
  cout << "***********************************************************" << endl;

}

/**
 *  ...
*/
void example4() {

  cout << "**** Example 4 ********************************************" << endl;

  Variable x(0), y(1), z(2);

  Poly flow = make_poly({ x>=1, y>=-1, y<=1, z>=-1, z<=1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

//   Poly a = make_poly({
//     closure_point(x + y), closure_point(-x + y), point(-x - y), point(x - y),
//     closure_point(3*y + 4*z),
//     ray(-y)
//   });
//   Poly b = make_poly({
//     closure_point(x + y), closure_point(-x + y),
//     point(x + y - 3*z), point(-x + y - 3*z),
//     ray(-y)
//   });
//   Poly c = make_poly({
//     point(3*y + 4*z),
//     closure_point(x + y), closure_point(-x + y), closure_point(x + y - 3*z),
// closure_point(-x + y - 3*z)
//   });

  Poly O = make_poly({ point() }, 3);

  Poly a = make_poly({
    closure_point(x + y), closure_point(-x + y), point(-x - y), point(x - y),
    closure_point(3*y + 4*z),
    ray(-y)
  });
  Poly b = make_poly({
    closure_point(x + y), closure_point(-x + y),
    point(x + y - 3*z), point(-x + y - 3*z),
    closure_point(x - y), closure_point(-x - y),
    ray(-y),
    ray(-5*x -2*z)
  });
  Poly c = make_poly({
    point(3*y + 4*z),
    closure_point(x + y), closure_point(-x + y), closure_point(x + y - 3*z),
    closure_point(-x + y - 3*z)
  });

  cout << "Poly a: " << a << endl;
  cout << "Poly b: " << b << endl;
  cout << "Poly c: " << c << endl;

  cout << "Generatori a: " << a.minimized_generators() << endl;
  cout << "Generatori b: " << b.minimized_generators() << endl;
  cout << "Generatori c: " << c.minimized_generators() << endl;

  Poly ac(a);
  ac.intersection_assign(c);
  cout << "Intersection a c: " << ac.minimized_generators() << endl;

  Poly ab(a);
  ab.intersection_assign(b);
  cout << "Intersection a b: " << ab.minimized_generators() << endl;

  Poly bc(b);
  bc.intersection_assign(c);
  cout << "Intersection b c: " << bc.minimized_generators() << endl;

  cout << "Boundary a - b: " << bndry(a, b) << endl;
  cout << "Boundary a - c: " << bndry(a, c) << endl;
  cout << "Boundary b - c: " << bndry(b, c) << endl;

  Loop loop({ a, b, c });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first 5 iterations of the algorithms
  computeLoop(flow, loop, 10);

  O.time_elapse_assign(reverse_flow);
  cout << "Flow cone" << O.minimized_generators() << endl;

  cout << "Final a " << loop[0] << endl;
  cout << "Final b " << loop[1] << endl;
  cout << "Final c " << loop[2] << endl;

  cout << "***********************************************************" << endl;

}

/**
 *  ...
*/
void example5() {

  cout << "**** Example 5 ********************************************" << endl;

  Variable x(0), y(1), z(2);

  Poly flow = make_poly({ point( x + y), point( -x + y ), point( z ) }, 3);
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

//   Poly a = make_poly({
//     closure_point(x + y), closure_point(-x + y), point(-x - y), point(x - y),
//     closure_point(3*y + 4*z),
//     ray(-y)
//   });
//   Poly b = make_poly({
//     closure_point(x + y), closure_point(-x + y),
//     point(x + y - 3*z), point(-x + y - 3*z),
//     ray(-y)
//   });
//   Poly c = make_poly({
//     point(3*y + 4*z),
//     closure_point(x + y), closure_point(-x + y), closure_point(x + y - 3*z),
// closure_point(-x + y - 3*z)
//   });

  Poly O = make_poly({ point() }, 3);

  Poly a = make_poly({
    point(), point( -x + 2*y ), ray(z)
  });
  Poly b = make_poly({
    closure_point(), point( x + 2*y ), ray(z)
  });
  Poly c = make_poly({
    closure_point(), closure_point( -x + 2*y ), closure_point( x + 2*y ), point( 2*y ), ray(z)
  });

  cout << "Poly a: " << a << endl;
  cout << "Poly b: " << b << endl;
  cout << "Poly c: " << c << endl;

  cout << "Generatori a: " << a.minimized_generators() << endl;
  cout << "Generatori b: " << b.minimized_generators() << endl;
  cout << "Generatori c: " << c.minimized_generators() << endl;

  Poly ac(a);
  ac.intersection_assign(c);
  cout << "Intersection a c: " << ac.minimized_generators() << endl;

  Poly ab(a);
  ab.intersection_assign(b);
  cout << "Intersection a b: " << ab.minimized_generators() << endl;

  Poly bc(b);
  bc.intersection_assign(c);
  cout << "Intersection b c: " << bc.minimized_generators() << endl;

  cout << "Boundary a - b: " << bndry(a, b) << endl;
  cout << "Boundary a - c: " << bndry(a, c) << endl;
  cout << "Boundary b - c: " << bndry(b, c) << endl;

  Loop loop({ a, c, b, c });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first ? iterations of the algorithms
  computeLoop(flow, loop, 3);

  // O.time_elapse_assign(reverse_flow);
  // cout << "Flow cone" << O.minimized_generators() << endl;
  //
  // cout << "Final a " << loop[0] << endl;
  // cout << "Final b " << loop[1] << endl;
  // cout << "Final c " << loop[2] << endl;

  cout << "***********************************************************" << endl;

}

/**
 *  ...
*/
void example6() {

  cout << "**** Example 6 * Tronco di piramide senza spessore *******************************************" << endl;

  Variable x(0), y(1), z(2);

  int mx = 1;
  int my = 1;
  int mz = 2;

  Poly flow = make_poly({ point( mx * x + my * y + mz * z ), point( -mx * x + my * y + mz * z ), point( -mx * x + -my * y + mz * z ), point( mx * x + -my * y + mz * z ) }, 3);
  Poly rotatedflow = make_poly({ point( my * y + mz * z ), point( -mx * x + mz * z ), point( -my * y + mz * z ), point( mx * x + mz * z ) }, 3);

  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

  Poly O = make_poly({ point() }, 3);

  Poly a = make_poly({
    point( 2*x + 2*y ), closure_point( 2*x - 2*y ), ray(1*x + 2*z), ray(1*x - 1*y + 2*z), ray(1*x + 1*y + 2*z),
  });
  Poly b = make_poly({
    point( 2*x - 2*y ), closure_point( -2*x - 2*y ), ray(-1*y + 2*z), ray(1*x - 1*y + 2*z), ray(-1*x - 1*y + 2*z)
  });
  Poly c = make_poly({
    point( -2*x - 2*y ), closure_point( -2*x + 2*y ), ray(-1*x + 2*z), ray(-1*x - 1*y + 2*z), ray(-1*x + 1*y + 2*z)
  });
  Poly d = make_poly({
    point( -2*x + 2*y ), closure_point( 2*x + 2*y ), ray(1*y + 2*z), ray(-1*x + 1*y + 2*z), ray(1*x + 1*y + 2*z)
  });

  cout << "Poly a: " << a << endl;
  cout << "Poly b: " << b << endl;
  cout << "Poly c: " << c << endl;
  cout << "Poly d: " << d << endl;

  cout << "Generatori a: " << a.minimized_generators() << endl;
  cout << "Generatori b: " << b.minimized_generators() << endl;
  cout << "Generatori c: " << c.minimized_generators() << endl;
  cout << "Generatori d: " << c.minimized_generators() << endl;

  // Poly ac(a);
  // ac.intersection_assign(c);
  // cout << "Intersection a c: " << ac.minimized_generators() << endl;
  //
  // Poly ab(a);
  // ab.intersection_assign(b);
  // cout << "Intersection a b: " << ab.minimized_generators() << endl;
  //
  // Poly bc(b);
  // bc.intersection_assign(c);
  // cout << "Intersection b c: " << bc.minimized_generators() << endl;
  //
  // cout << "Boundary a - b: " << bndry(a, b) << endl;
  // cout << "Boundary a - c: " << bndry(a, c) << endl;
  // cout << "Boundary b - c: " << bndry(b, c) << endl;

  Loop loop({ a, b, c, d });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first ? iterations of the algorithms
  computeLoop(flow, loop, 3);

  // O.time_elapse_assign(reverse_flow);
  // cout << "Flow cone" << O.minimized_generators() << endl;
  //
  // cout << "Final a " << loop[0] << endl;
  // cout << "Final b " << loop[1] << endl;
  // cout << "Final c " << loop[2] << endl;

  cout << "***********************************************************" << endl;

}

/**
 *  ...
*/
void example7() {

  cout << "**** Example 7 * Tronco di piramide con spessore *******************************************" << endl;

  Variable x(0), y(1), z(2);

  int mx = 4;
  int my = 4;
  int mz = 1;

  Poly flow = make_poly({ point( my * y + mz * z ), point( -mx * x + mz * z ), point( -my * y + mz * z ), point( mx * x + my * z ) }, 3);

  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

  Poly O = make_poly({ point() }, 3);

  Poly a = make_poly({
    point( 2*x + 2*y ), closure_point( 2*x - 2*y ), ray(1*x + 2*z), ray(1*x - 1*y + 2*z), ray(1*x + 1*y + 2*z),
    point( 1*x + 1*y ), closure_point( 1*x - 1*y )
  });
  Poly b = make_poly({
    point( 2*x - 2*y ), closure_point( -2*x - 2*y ), ray(-1*y + 2*z), ray(1*x - 1*y + 2*z), ray(-1*x - 1*y + 2*z),
    point( 1*x - 1*y ), closure_point( -1*x - 1*y )
  });
  Poly c = make_poly({
    point( -2*x - 2*y ), closure_point( -2*x + 2*y ), ray(-1*x + 2*z), ray(-1*x - 1*y + 2*z), ray(-1*x + 1*y + 2*z),
    point( -1*x - 1*y ), closure_point( -1*x + 1*y )
  });
  Poly d = make_poly({
    point( -2*x + 2*y ), closure_point( 2*x + 2*y ), ray(1*y + 2*z), ray(-1*x + 1*y + 2*z), ray(1*x + 1*y + 2*z),
    point( -1*x + 1*y ), closure_point( 1*x + 1*y )
  });

  cout << "Poly a: " << a << endl;
  cout << "Poly b: " << b << endl;
  cout << "Poly c: " << c << endl;
  cout << "Poly d: " << d << endl;

  cout << "Generatori a: " << a.minimized_generators() << endl;
  cout << "Generatori b: " << b.minimized_generators() << endl;
  cout << "Generatori c: " << c.minimized_generators() << endl;
  cout << "Generatori d: " << c.minimized_generators() << endl;

  // Poly ac(a);
  // ac.intersection_assign(c);
  // cout << "Intersection a c: " << ac.minimized_generators() << endl;
  //
  // Poly ab(a);
  // ab.intersection_assign(b);
  // cout << "Intersection a b: " << ab.minimized_generators() << endl;
  //
  // Poly bc(b);
  // bc.intersection_assign(c);
  // cout << "Intersection b c: " << bc.minimized_generators() << endl;
  //
  // cout << "Boundary a - b: " << bndry(a, b) << endl;
  // cout << "Boundary a - c: " << bndry(a, c) << endl;
  // cout << "Boundary b - c: " << bndry(b, c) << endl;

  Loop loop({ a, b, c, d });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first ? iterations of the algorithms
  Loop last = computeLoop(flow, loop, 3);

  cout << "Is loop equal to last? " << ((last == loop) ? "Yes" : "No") << endl;

  // O.time_elapse_assign(reverse_flow);
  // cout << "Flow cone" << O.minimized_generators() << endl;
  //
  // cout << "Final a " << loop[0] << endl;
  // cout << "Final b " << loop[1] << endl;
  // cout << "Final c " << loop[2] << endl;

  cout << "***********************************************************" << endl;

}


void example8() {

  cout << "**** Example 8 * Due loop feasible che ricevono dei tagli prima di convergere ********" << endl;

  Variable x(0), y(1), z(2);

  Poly flow1 = make_poly({ point( -y + z ), point( y + z )}, 3);

  cout << "Vincoli del Flusso: " << flow1 << endl;
  cout << "Punti del Flusso: " << flow1.minimized_generators() << endl;

  Poly a = make_poly({
      point( -x ), point( 2*x ), point( -x + 3*y ), point( 2*x + 3*y), ray(z)
  });
  Poly b = make_poly({
      closure_point( -2*x ), closure_point( x ), point( -2*x - 3*y ), point( x - 3*y), ray(z)
  });

  cout << "Poly a: " << a << endl;
  cout << "Poly b: " << b << endl;
  cout << "Generatori a: " << a.minimized_generators() << endl;
  cout << "Generatori b: " << b.minimized_generators() << endl;

  Loop loop({ a, b });
  cout << "Loop: " << loop << endl;
  cout << "Is proper loop: " << is_proper_loop(loop) << endl;

  // Show the first ? iterations of the algorithms
  Loop last = computeLoop(flow1, loop, 3);

  cout << "Is loop equal to last? " << ((last == loop) ? "Yes" : "No") << endl;

  cout << "***********************************************************" << endl;

  Poly flow2 = make_poly({ point( x -y +z ), point( x +y +z ), point( -x +y +z ), point( -x -y +z ),
	                   point( x -y +2*z ), point( x +y +2*z ), point( -x +y +2*z ), point( -x -y +2*z )}, 3);

  cout << "Vincoli del Flusso: " << flow2 << endl;

  Poly a2 = make_poly({
      point( -x ), point( 2*x ), point( -x + 3*y ), point( 2*x + 3*y), ray(x+z)
  });
  Poly b2 = make_poly({
      closure_point( -2*x ), closure_point( x ), point( -2*x - 3*y ), point( x - 3*y), ray(x+z)
  });

  Loop loop2({ a2, b2 });
  cout << "Loop: " << loop2 << endl;
  cout << "Is proper loop: " << is_proper_loop(loop2) << endl;

  // Show the first ? iterations of the algorithms
  Loop last2 = computeLoop(flow2, loop2, 3);

  cout << "***********************************************************" << endl;
}

void example9() {

  Variable x(0), y(1);

  Poly flow  = make_poly({ x>=-5, x<=5, y==1});
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  cout << "Vincoli del Flusso: " << flow << endl;
  cout << "Punti del Flusso: " << flow.minimized_generators() << endl;

  Poly B = make_poly({ closure_point(x+y), closure_point(x+2*y),
    closure_point(4*x+y), closure_point(4*x+2*y), point(2*x+2*y) });

  Poly A1 = make_poly({ point(1*x), point(1*x+1*y), point(2*x+1*y), point(2*x) });
  Poly A2 = make_poly({ point(3*x), point(3*x+3*y), point(4*x+3*y), point(4*x) });
  Poly A3 = make_poly({ closure_point(1*x), point(1*x-1*y), point(4*x-1*y), closure_point(4*x) });

  PPS A(2,EMPTY);
  A.add_disjunct(A1);
  A.add_disjunct(A2);
  A.add_disjunct(A3);

  vector<pair<Poly, Poly>> Result = reach0(A,B,reverse_flow);

  for (auto pair: Result) {
    cout << "Patch: " << pair.first << endl;
    cout << "Poly: " << pair.second << endl;
  }
}

void example_LTLf_Phi1() {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
     p1 = (a>=0 and b>=0)
     p2 = (b<10 and t<=10)
   */
  Variable a(0), b(1), t(2);

  Poly flow = make_poly({ a>=-1, a<=1, b>=-2, b<=2, a+b>=-2, a+b<=2, t==1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(3, EMPTY), p1(3, EMPTY), p2(3, EMPTY);
  p1.add_disjunct(make_poly({ a>=0, b>=0 }, 3));
  p2.add_disjunct(make_poly({ b<10, t<=10 }, 3));
  State2PPS polysys = { DUMMY, p2, p1 };

  PPS result = ltlf_mc(Automata::Phi1, { .dimension = 3, .preflow = reverse_flow, .AP2PPS = polysys });
  cout << endl << result << endl;
}

void example2_LTLf_Phi1() {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
     p1 = (a>=0 and b>=0)
     p2 = (b<10 and t<=10)
   */

  const int dim = 2;

  Variable b(0), t(1);

  Poly flow = make_poly({ b>=-2, b<=2, t==1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(dim, EMPTY), p1(dim, EMPTY), p2(dim, EMPTY);
  p1.add_disjunct(make_poly({ b>=0 }, dim));
  p2.add_disjunct(make_poly({ b<=10, t<=10 }, dim));
  State2PPS polysys = { DUMMY, p2, p1 };

  PPS result = ltlf_mc(Automata::Phi1, { .dimension = dim, .preflow = reverse_flow, .AP2PPS = polysys });
  cout << endl << result << endl;
}
