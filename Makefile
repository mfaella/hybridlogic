CPPFLAGS=-ggdb -Wall

LIBS=-lppl -lgmp

OBJS=basics.o loop.o pairwise_reduce.o rwa_maps.o reach.o ltlf_mc.o

ALL: $(OBJS) main.o
	g++ -o loops $(OBJS) $(LIBS)

fossacs: $(OBJS) fossacs.o
	g++ -o fossacs $(OBJS) fossacs.o $(LIBS)

clean:
	rm *.o
