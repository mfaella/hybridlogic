
#include <exception>
#include "reach.hpp"

ReachPairs reach0(const PPS & A, const Poly & B, const Poly & preflow) {

  Poly PreB = B;
  Poly ClsB = B;
  PreB.positive_time_elapse_assign(preflow);
  ClsB.topological_closure_assign();
  PreB.intersection_assign(ClsB);

  ReachPairs Result;
  for (PPS::const_iterator p_it = A.begin(); p_it != A.end(); p_it++) {
    const Poly & Q = p_it->pointset();
    Poly Y = Q;
    Y.intersection_assign(PreB);
    if (!Y.is_empty()) {
      pair<Poly, Poly> pair;
      pair.first = Q;
      pair.second = std::move(Y);
      Result.push_back(std::move(pair));
    }
  }

  return Result;
}

/* Returns the set of points of A that can reach B after a positive delay,
   while staying in A at all intermediate times. */
ReachPairs reach_plus(const PPS & A, const Poly & B, const Poly & preflow) {

  PPS Delta(A.space_dimension(), EMPTY);
  PPS NotA(A.space_dimension());
  NotA.difference_assign(A);

  // cout << endl << "A: " << A << endl;
  // cout << "B: " << B << endl;

  /* Two differences between rwa(B, NotA) and reach+(A,B):
     1) reach+ requires positive delay
     2) ??
   */
  for (PPS::const_iterator p_it = A.begin(); p_it != A.end(); p_it++) {

    Poly U = p_it->pointset();
    // cout << "Patch of A: " << U << endl;
    U.topological_closure_assign();
    U.intersection_assign(B);
    // cout << "Boundary: " << U << endl;
    U.positive_time_elapse_assign(preflow);
    U.intersection_assign(p_it->pointset());

    // cout << "U: " << U << endl;
    PPS UU(std::move(U));

    auto X = rwa_maps(UU, NotA, preflow);
    // cout << "X: " << X.first << endl;

    add_powerset(Delta, X.first);
  }

  // Find the A-patch of each part of Delta
  ReachPairs Result;

  for (PPS::const_iterator Delta_it = Delta.begin(); Delta_it != Delta.end(); Delta_it++) {
    bool patch_found = false;
    const Poly & Y = Delta_it->pointset();
    for (PPS::const_iterator A_it = A.begin(); A_it != A.end(); A_it++) {
      const Poly & Q = A_it->pointset();
      Poly Y2 = Y;
      Y2.intersection_assign(Q);
      if (!Y2.is_empty()) {
        patch_found = true;
        pair<Poly, Poly> pair;
        pair.first = Q;
        pair.second = Y2;
        Result.push_back(std::move(pair));
      }
    }
    if (!patch_found) {

      cout << "A: " << A << endl;
      cout << "B: " << B << endl;
      cout << "Delta: " << Delta_it->pointset() << endl;

      throw runtime_error("ERROR: Patch not found!");
    }
  }
  return Result;
}
