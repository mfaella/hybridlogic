#include "basics.h"

void insert_all(Constraint_System &sys, const vector<Constraint> &constraints) {
  for (auto c=constraints.begin(); c!=constraints.end(); c++) {
      sys.insert(*c);
  }
}

void print_a_point(const Polyhedron& p) {
  if (p.is_empty()) {
    cout << "Polyhedron is empty." << endl;
    return;
  }
  Generator_System sys = p.generators();
  for (auto gen: sys) {
    if (gen.is_point()) {
      cout << "Witness: " << gen << endl;
      return;
    }
  }
}

Poly make_poly(const vector<Constraint> &constraints, int dim) {
  Constraint_System sys;
  if (dim>0)
    sys.set_space_dimension(dim);
  insert_all(sys, constraints);
  return Poly(sys);
}

Poly make_poly(const vector<Generator> &points) {
  Generator_System sys;
  for(auto &point : points) {
    sys.insert(point);
  }
  return Poly(sys);
}

Poly make_poly(const vector<Generator> &points, int dim) {
  Generator_System sys;
  sys.set_space_dimension(dim);
  for(auto &point : points) {
    sys.insert(point);
  }
  return Poly(sys);
}

/*
AGGIUNGE AL POWERSET FIRST IL POWERSET SECOND
*/
void add_powerset(PPS &first, const PPS &second) {
  PPS::const_iterator it;

  for (it=second.begin(); it!=second.end(); it++)
    if (!(it->pointset().is_empty()))
      first.add_disjunct(it->pointset());
}



// Funzione che fa le veci della vecchia continuous_set_PPL_NNC::negate(), serve per negare il PS.
void negate(PPS& to_negate){

  //	using IO_Operators::operator<<;

	PPS p_neg(to_negate.space_dimension(), Parma_Polyhedra_Library::UNIVERSE);
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
	p_neg.difference_assign(to_negate);
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
	// fast_pairwise_reduce(p_neg);
	p_neg.pairwise_reduce();
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
	to_negate = p_neg;
	//cout << "to_negate = " << to_negate << "p_neg = " << p_neg << endl;
}


// Inversion w.r.t. the origin
void prepoly_assign(Poly& my_poly)
{
    // maps all points componentwise by x'=-x
    for (unsigned int i=0; i<my_poly.space_dimension(); ++i)
    {
      Linear_Expression lexp=-Variable(i);
      my_poly.affine_image(Variable(i),lexp);
    }
}


/*
 * affine_hull restituisce l'affine hull del poliedro ph preso in input
 *
 *  INPUT:	ph 	(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare l'affine hull;
 *  OUTPUT:	ahull	(NNC_Polyhedron)	-> affine hull di ph;
 *
 */
Poly affine_hull(const Poly &ph)
{
  //dimensione spaziale del poliedro in input
  dimension_type dim_space=ph.space_dimension();

  //dimensione affine del poliedro in input
  dimension_type dim_affine=ph.affine_dimension();

  //se ph è  vuoto restituisce l'insieme vuoto, ossia ph stesso
  if((ph.is_empty()))
  {
    //poliedro vuoto
    return ph;
  }
  else
  {
    Poly ahull(dim_space,UNIVERSE);
    //se invece è uguale alla dimensione spaziale restituisce l'insieme universo
    if (dim_affine==dim_space)
    {
      return ahull;
    }
    else
    //altrimenti
    //restituisce l'insieme dei vincoli di uguaglianza di ph poiché
    //la loro intersezione è uguale a un poliedro avente stessa
    //dimensione affine di ph
    {
	const Constraint_System& cs=ph.constraints();
	for (Constraint_System::const_iterator i = cs.begin(),cs_end = cs.end();i != cs_end; ++i)
	  if (i->is_equality())
		 ahull.add_constraint(*i);
	return ahull;
    }
  }
}


/*
 * relative_interior restituisce il relative interior del poliedro preso in input
 *
 *  INPUT:   ph 			(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare
 * 					   	   il relative interior;
 *  OUTPUT:  ph_relative_interior	(NNC_Polyhedron)	-> relative interior di ph;
 *
 */
Poly relative_interior(const Poly &ph)
{
  //sistema di vincoli di ph
  const Constraint_System& cs = ph.constraints();
  //sistema di vincoli del relative interior di ph
  Constraint_System new_cs;
  //cs.set_space_dimension(ph.space_dimension());

  for (Constraint_System::const_iterator i = cs.begin(), cs_end = cs.end(); i != cs_end; ++i)
  {
    Constraint c=*i;
    //se sono vincoli > o = vengono aggiunti così come sono a new_cs
    if (c.is_strict_inequality()||c.is_equality())
      new_cs.insert(c);
    else
    //altrimenti
    //si tratta di un vincolo >= che viene convertito in >
    {
      // Linear_Expression e(c);
      // Utilizzato il nuovo operatore, Urbano.
      Linear_Expression e(c.expression());

      Constraint strict_ineq_c=(e>0);
      new_cs.insert(strict_ineq_c);
    }
  }

  Poly ph_relative_interior(new_cs);
  return ph_relative_interior;
}


/*
 * zero_mirror_assign trasforma il poliedro passato nella sua riflessione rispetto l'origine
 * INPUT:   ph 	(NNC_Polyhedron)	-> poliedro da riflettere;
 * OUTPUT:  ph	(NNC_Polyhedron)	-> riflessione di ph rispetto l'origine;
 *
 */
void zero_mirror_assign(Poly &ph)
{
  Linear_Expression lexp;
  for (unsigned int i=0; i<ph.space_dimension(); ++i)
  {
    lexp=-Variable(i);
    //trasforma ogni x appartenente a ph in -x
    ph.affine_image(Variable(i),lexp);
  }

}


/*
 * point_mirror restituisce la riflessione di ph rispetto al punto x,
 * cioé 2*x-y, con y appartenente a ph
 *
 * INPUT:	ph 	(NNC_Polyhedron)	-> poliedro da riflettere;
 *		x	(Generator)	-> punto rispetto al quale riflettere;
 * OUTPUT:	my_ph 	(NNC_Polyhedron)	-> riflessione di ph rispetto a x
 *
 */
Poly point_mirror(const Poly &ph, const Generator &x)
{
  Linear_Expression lexp;
  GMP_Integer denominator;
  Poly my_ph(ph);
  //per ogni punto y appartenete a ph...
  for (unsigned int i=0; i<ph.space_dimension(); ++i)
  {
    //...y -> 2*x-y
    lexp=2*x.coefficient(Variable(i))-Variable(i)*x.divisor();
    //denominatore del punto x
    denominator=x.divisor();
    my_ph.affine_image(Variable(i),lexp,denominator);
  }
  return my_ph;
}



/*
 * bndry(ph1,ph2) = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 *
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *   OUTPUT:  output    (NNC_Polyhedron) -> output = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 *
 */
Poly bndry(const Poly &ph1,const Poly &ph2)
{
  //conta_bndry++; COMMENTATO DA MARCO.
  //copia ph1
  Poly output(ph1);
  //copia ph2
  Poly cl_ph2(ph2);

  //cl(ph1) (chiusura topologica di ph1)
  output.topological_closure_assign();

  //cl(ph2)
  cl_ph2.topological_closure_assign();

  //cl(ph1) /\ ph2 (cl(ph1) intersecato ph2)
  output.intersection_assign(ph2);

  //cl(ph2) /\ ph1
  cl_ph2.intersection_assign(ph1);

  //(cl(ph1) /\ ph2)\/(cl(ph2) /\ ph1) ((cl(ph1) /\ ph2) unito (cl(ph2) /\ ph1))
  output.upper_bound_assign_if_exact(cl_ph2);

  return output;
}


//CALCOLA BNDRY(P1,P2)
Poly simple_border_opt(Poly &poly1_nnc, Poly &poly1close_nnc, Poly &poly2_nnc)
{
//Calcola bndry(p1,p2)
  Poly border_nnc(poly1close_nnc);

  border_nnc.intersection_assign(poly2_nnc);
  if (border_nnc.is_empty())
    {
      Poly closure2_nnc(poly2_nnc);
      closure2_nnc.topological_closure_assign();
      closure2_nnc.intersection_assign(poly1_nnc);
      return closure2_nnc;
    }
  return border_nnc;
}


//CALCOLA ENTRY(INTERNAL_POLY_NNC, EXTERNAL_POLY_NNC)
Poly exit_border_opt(Poly &internal_poly_nnc, Poly &internal_polyclose_nnc, Poly &external_poly_nnc, Poly &flow_nnc)
{
  //calcola entry(p1,p2)
  Poly border_nnc=bndry(internal_poly_nnc, external_poly_nnc);
  if (!border_nnc.is_empty())
    {
      Poly elapse_nnc(external_poly_nnc);
      elapse_nnc.time_elapse_assign(flow_nnc);
      border_nnc.intersection_assign(elapse_nnc);
    }
  return border_nnc;
}



// Aggiunge a [b] tutti i poliedri in [a], aggiungendo a ciascuno il generatore [gen].
static void generator_time_elapse(PPS &a, PPS &b,
				 const Parma_Polyhedra_Library::Generator &gen)
{
  for (PPS::iterator p_it = a.begin(); p_it != a.end(); p_it++) {
    Poly p = p_it->pointset();
    p.add_generator(gen);
    b.add_disjunct(p);
  }
}


using std::vector;
typedef vector<PPS> list_powerset_type;


// RIMUOVE LA PARTE UNBOUNDED DI [a_ps] RISPETTO [postflow_nnc]
void remove_unbounded(PPS &a_ps, Poly postflow_nnc, Poly preflow_nnc)
{
	dimension_type dim = a_ps.space_dimension();
	PPS::iterator it;
	Poly p_nnc(dim);

	list_powerset_type to_be_added;

	postflow_nnc.topological_closure_assign();
	// For each convex P in [a_ps]
	for (it = a_ps.begin(); it != a_ps.end();)
    {
		p_nnc = it->pointset();

		// If P is not bounded
		if (!is_bounded(p_nnc, postflow_nnc))
		{
		  // cut the positive pre-flow of P from P
		  Poly ptime_nnc(p_nnc);
		  ptime_nnc.positive_time_elapse_assign(preflow_nnc);
			PPS p_ps(p_nnc), ptime_ps(ptime_nnc);
			p_ps.difference_assign(ptime_ps);
			it = a_ps.drop_disjunct(it);
			to_be_added.push_back(p_ps);
		}
		else
			it++;
	}

	for (list_powerset_type::iterator l_it=to_be_added.begin();l_it!=to_be_added.end();l_it++)
	  add_powerset(a_ps, *l_it);

	return;
}


// RITORNA TRUE SSE [p_nnc] E' BOUNDED RISPETTO [flow_nnc].
bool is_bounded(const Poly &p_nnc, const Poly &flow_nnc)
{
  dimension_type dim=p_nnc.space_dimension();
  Constraint_System cs;
  Generator_System gs;

  const Poly &origin_nnc = get_origin(dim);

  // If Origin belongs to the flow then P is not bounded
  if (flow_nnc.contains(origin_nnc))
    return false;

  // Build characteristic cone of P, i.e.,
  // add the rays of P to the Origin
  Poly cone_nnc(origin_nnc);
  gs=p_nnc.generators();

  for (Generator_System::const_iterator gen=gs.begin(); gen!=gs.end(); ++gen)
    if (gen->is_ray()) { // add ray [gen] to Origin
      // cout << "Adding ray " << *gen << endl;
      cone_nnc.add_generator(*gen);
    } else if (gen->is_line()) {


      //Linear_Expression le(*gen);
      // La precedente riga di codice commentata è stata sostituita dalla riga
      // successiva a questo commento con il nuovo metodo di PPL.
      Linear_Expression le(gen->expression());
      // Add first ray of the line
      cone_nnc.add_generator(ray(le));
      // Add second ray of the line
      cone_nnc.add_generator(ray(-le));
    }
  cone_nnc.intersection_assign(flow_nnc);

  if (cone_nnc.is_empty())
    return true;

  return false;
}

//RITORNA IL POLIEDRO ORIGINE DI DIMENSIONE DIM
Poly get_origin(int dim)
{
  Constraint_System cs;

  for (int i=0; i<dim; i++) {
    Variable var(i);
    cs.insert(var==0);
  }

  Poly origin(cs);
  return origin;
}


// Tries to simplify each convex polyhedron in [ps].
void simplify(PPS &ps)
{
  PPS::iterator it;
  // For each convex P in [ps]
  for (it = ps.begin(); it != ps.end(); it++)
	  it->pointset().minimized_constraints();
}
