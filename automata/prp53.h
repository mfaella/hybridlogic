namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('b>a', 'sing', 'a>b', 't<=T', 'a>=0 and b>=0', 't=0')
    Automaton Aut53 = {
        .Predecessor = {
            {18, 12},
            {},
            {17, 10},
            {6, 14},
            {2, 11},
            {18, 12},
            {17, 10},
            {0, 16},
            {1, 15},
            {13, 5},
            {6, 14},
            {3, 4, 9, 20, 21},
            {13, 5},
            {8},
            {19, 7},
            {8},
            {19, 7},
            {0, 16},
            {1, 15},
            {13, 5},
            {0, 16},
            {1, 15},
            {17, 10},
            {3, 4, 9, 20, 21}},
        .Initial = {1},
        .Final = {22, 23},
        .Label = {{TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::DontC, TFD::True}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}}};
}
