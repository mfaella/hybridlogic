namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('b>a', 'sing', 'a>b', 'a>=0 and b>=0', 't=0')
    Automaton Aut33 = {
        .Predecessor = {
            {10, 13},
            {18, 14},
            {},
            {5, 6, 11, 12, 17},
            {10, 13},
            {15, 7},
            {9, 2},
            {4, 20},
            {4, 20},
            {21},
            {21},
            {1, 3},
            {8, 19},
            {0, 16},
            {15, 7},
            {0, 16},
            {9, 2},
            {10, 13},
            {8, 19},
            {18, 14},
            {15, 7},
            {9, 2},
            {18, 14},
            {5, 6, 11, 12, 17}},
        .Initial = {2},
        .Final = {22, 23},
        .Label = {{TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::True}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}}};
}
