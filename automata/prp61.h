namespace Automata {
using TFD = Automaton::TFD;

// Atomic props: ('q', 'sing', 'p', 'inv')
Automaton Aut61 = {
   .Predecessor = {
       {},        // 0
       {0, 10},   // 1
       {11, 5},   // 2
       {1, 2, 7}, // 3
       {8, 9},    // 4
       {8, 9},    // 5
       {0, 10},   // 6
       {3, 4},    // 7
       {0, 10},   // 8
       {11, 5},   // 9
       {6},       // 10
       {6},       // 11
       {1, 2, 7}, // 12
       {8, 9}     // 13
   },
   .Initial = {0} ,
   .Final = {12, 13} ,
   .Label = {
       {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC},     // 0:   1->4
       {TFD::True, TFD::False, TFD::True, TFD::True},       // 1:   4->6
       {TFD::True, TFD::False, TFD::DontC, TFD::True},      // 2:   5->6
       {TFD::DontC, TFD::True, TFD::DontC, TFD::True},      // 3:   6->7
       {TFD::True, TFD::True, TFD::DontC, TFD::True},       // 4:   3->7
       {TFD::False, TFD::True, TFD::DontC, TFD::True},      // 5:   3->5
       {TFD::DontC, TFD::False, TFD::False, TFD::True},     // 6:   4->2
       {TFD::DontC, TFD::False, TFD::DontC, TFD::True},     // 7:   7->6
       {TFD::False, TFD::False, TFD::True, TFD::True},      // 8:   4->3
       {TFD::False, TFD::False, TFD::DontC, TFD::True},     // 9:   5->3
       {TFD::DontC, TFD::True, TFD::False, TFD::True},      // 10:  2->4
       {TFD::DontC, TFD::True, TFD::True, TFD::True},       // 11:  2->5
       {TFD::DontC, TFD::True, TFD::DontC, TFD::True},      // 12:  6Acc
       {TFD::True, TFD::True, TFD::DontC, TFD::True}        // 13:  3Acc
   }
};
}
