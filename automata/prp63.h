namespace Automata {
    using TFD = Automaton::TFD;

    // Atomic props: ('q', 'sing', 'p', 'inv')
    Automaton Aut63 = {
       .Predecessor = {
           {8, 6},
           {12, 21},
           {14, 7},
           {4, 9, 10, 11, 15},
           {19, 20},
           {8, 6},
           {5},
           {5},
           {},
           {17, 3},
           {8, 6},
           {12, 21},
           {1, 13},
           {19, 20},
           {0, 16},
           {14, 7},
           {14, 7},
           {1, 13},
           {19, 20},
           {0, 16},
           {2, 18},
           {2, 18},
           {1, 13},
           {4, 9, 10, 11, 15}
       },
       .Initial = {8} ,
       .Final = {22, 23} ,
       .Label = {
           {TFD::False, TFD::False, TFD::True, TFD::True},
           {TFD::False, TFD::False, TFD::DontC, TFD::True},
           {TFD::True, TFD::False, TFD::False, TFD::True},
           {TFD::DontC, TFD::True, TFD::DontC, TFD::True},
           {TFD::True, TFD::False, TFD::True, TFD::True},
           {TFD::DontC, TFD::False, TFD::False, TFD::True},
           {TFD::DontC, TFD::True, TFD::False, TFD::True},
           {TFD::DontC, TFD::True, TFD::True, TFD::True},
           {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC},
           {TFD::DontC, TFD::False, TFD::DontC, TFD::True},
           {TFD::True, TFD::False, TFD::True, TFD::True},
           {TFD::True, TFD::False, TFD::DontC, TFD::True},
           {TFD::False, TFD::True, TFD::DontC, TFD::True},
           {TFD::False, TFD::False, TFD::True, TFD::True},
           {TFD::False, TFD::True, TFD::DontC, TFD::True},
           {TFD::True, TFD::False, TFD::True, TFD::True},
           {TFD::False, TFD::False, TFD::DontC, TFD::True},
           {TFD::True, TFD::True, TFD::DontC, TFD::True},
           {TFD::DontC, TFD::False, TFD::False, TFD::True},
           {TFD::True, TFD::True, TFD::DontC, TFD::True},
           {TFD::DontC, TFD::True, TFD::False, TFD::True},
           {TFD::DontC, TFD::True, TFD::True, TFD::True},
           {TFD::True, TFD::True, TFD::DontC, TFD::True},
           {TFD::DontC, TFD::True, TFD::DontC, TFD::True}
       }
    };
}
