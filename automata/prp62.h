namespace Automata {
    using TFD = Automaton::TFD;

    // Atomic props: ('p', 'sing', 'q', 'inv')
    Automaton Aut62 = {
       .Predecessor = {
           {2, 6},
           {16, 3},
           {16, 3},
           {1, 14},
           {11, 5},
           {9, 10, 4, 7},
           {0, 15},
           {1, 14},
           {},
           {8, 13},
           {2, 6},
           {0, 15},
           {8, 13},
           {12},
           {12},
           {1, 14},
           {8, 13},
           {0, 15},
           {9, 10, 4, 7}
       },
       .Initial = {8} ,
       .Final = {17, 18} ,
       .Label = {
           {TFD::False, TFD::False, TFD::DontC, TFD::True},
           {TFD::DontC, TFD::True, TFD::False, TFD::True},
           {TFD::DontC, TFD::True, TFD::True, TFD::True},
           {TFD::DontC, TFD::False, TFD::False, TFD::True},
           {TFD::DontC, TFD::False, TFD::DontC, TFD::True},
           {TFD::DontC, TFD::True, TFD::DontC, TFD::True},
           {TFD::False, TFD::True, TFD::DontC, TFD::True},
           {TFD::True, TFD::False, TFD::True, TFD::True},
           {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC},
           {TFD::True, TFD::False, TFD::True, TFD::True},
           {TFD::True, TFD::False, TFD::DontC, TFD::True},
           {TFD::True, TFD::True, TFD::DontC, TFD::True},
           {TFD::False, TFD::False, TFD::DontC, TFD::True},
           {TFD::False, TFD::True, TFD::DontC, TFD::True},
           {TFD::True, TFD::True, TFD::DontC, TFD::True},
           {TFD::False, TFD::False, TFD::True, TFD::True},
           {TFD::True, TFD::False, TFD::False, TFD::True},
           {TFD::True, TFD::True, TFD::DontC, TFD::True},
           {TFD::DontC, TFD::True, TFD::DontC, TFD::True}
       }
    };
}
