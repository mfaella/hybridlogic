namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('a>b', 'sing', 'b>a', 'a>=0 and b>=0', 't=0')
    Automaton Aut32 = {
        .Predecessor = {
            {3, 4},
            {3, 4},
            {16},
            {0, 2},
            {12, 5},
            {},
            {9, 15, 13, 7},
            {11, 6},
            {1, 10},
            {0, 2},
            {8, 14},
            {8, 14},
            {16},
            {12, 5},
            {0, 2},
            {1, 10},
            {12, 5},
            {8, 14},
            {9, 15, 13, 7}},
        .Initial = {5},
        .Final = {17, 18},
        .Label = {{TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::True}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}}};
}
