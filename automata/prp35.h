namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('b>a', 'sing', 'a>b', 'a>=0 and b>=0', 't=0')
    Automaton Aut35 = {
        .Predecessor = {
            {21, 15},
            {16, 4},
            {12, 30},
            {29, 6},
            {1, 13},
            {12, 30},
            {9, 10},
            {28, 31},
            {},
            {8, 27},
            {28, 31},
            {1, 13},
            {17, 5},
            {21, 15},
            {12, 30},
            {14, 22},
            {14, 22},
            {29, 6},
            {8, 27},
            {16, 4},
            {28, 31},
            {17, 5},
            {21, 15},
            {0, 2, 3, 7, 19, 24, 26},
            {11, 23},
            {29, 6},
            {8, 27},
            {18},
            {18},
            {25, 20},
            {25, 20},
            {9, 10},
            {1, 13},
            {0, 2, 3, 7, 19, 24, 26}},
        .Initial = {8},
        .Final = {32, 33},
        .Label = {{TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::True}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}}};
}
