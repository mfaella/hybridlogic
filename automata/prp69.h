namespace Automata {
using TFD = Automaton::TFD;

// Atomic props: ('q', 'sing', 'p', 'inv')
Automaton Aut69 = {
   .Predecessor = {
       {49, 2},
       {51, 11},
       {34, 20},
       {26, 43},
       {28, 47},
       {39, 23},
       {9, 3},
       {17, 12},
       {40, 21},
       {28, 47},
       {29, 31},
       {33, 5},
       {33, 5},
       {48, 27},
       {34, 20},
       {6, 30},
       {26, 43},
       {44, 22},
       {40, 21},
       {0, 1, 32, 35, 4, 36, 37, 38, 42, 13, 46},
       {49, 2},
       {10, 18},
       {51, 11},
       {10, 18},
       {28, 47},
       {6, 30},
       {16},
       {44, 22},
       {16},
       {45, 15},
       {24, 25},
       {24, 25},
       {26, 43},
       {51, 11},
       {48, 27},
       {39, 23},
       {17, 12},
       {40, 21},
       {29, 31},
       {8, 41},
       {45, 15},
       {39, 23},
       {19, 14},
       {},
       {17, 12},
       {29, 31},
       {6, 30},
       {9, 3},
       {50, 7},
       {50, 7},
       {48, 27},
       {8, 41},
       {0, 1, 32, 35, 4, 36, 37, 38, 42, 13, 46},
       {34, 20}
   },
   .Initial = {43} ,
   .Final = {52, 53} ,
   .Label = {
       {TFD::True, TFD::False, TFD::DontC, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::False, TFD::True, TFD::DontC, TFD::True},
       {TFD::False, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::False, TFD::True},
       {TFD::True, TFD::True, TFD::DontC, TFD::True},
       {TFD::True, TFD::False, TFD::False, TFD::True},
       {TFD::False, TFD::False, TFD::True, TFD::True},
       {TFD::False, TFD::False, TFD::DontC, TFD::True},
       {TFD::True, TFD::False, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::True, TFD::DontC, TFD::True},
       {TFD::False, TFD::False, TFD::True, TFD::True},
       {TFD::DontC, TFD::False, TFD::False, TFD::True},
       {TFD::False, TFD::True, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::False, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::DontC, TFD::True},
       {TFD::False, TFD::False, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::True, TFD::False, TFD::True},
       {TFD::False, TFD::False, TFD::True, TFD::True},
       {TFD::DontC, TFD::True, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::False, TFD::True},
       {TFD::DontC, TFD::False, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::False, TFD::True},
       {TFD::True, TFD::True, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::True, TFD::True, TFD::True},
       {TFD::False, TFD::True, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::True, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::DontC, TFD::False, TFD::False, TFD::True},
       {TFD::False, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::False, TFD::True, TFD::DontC, TFD::True},
       {TFD::True, TFD::True, TFD::DontC, TFD::True},
       {TFD::False, TFD::False, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::False, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC},
       {TFD::False, TFD::False, TFD::DontC, TFD::True},
       {TFD::False, TFD::False, TFD::DontC, TFD::True},
       {TFD::True, TFD::False, TFD::True, TFD::True},
       {TFD::False, TFD::True, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::True, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::True, TFD::True},
       {TFD::DontC, TFD::False, TFD::False, TFD::True},
       {TFD::True, TFD::True, TFD::DontC, TFD::True},
       {TFD::DontC, TFD::True, TFD::DontC, TFD::True},
       {TFD::True, TFD::True, TFD::DontC, TFD::True}
   }
};
}
