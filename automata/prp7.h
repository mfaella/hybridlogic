namespace Automata {
using TFD = Automaton::TFD;

// Atomic props: ('q', 'sing', 'p')
Automaton Aut7 = {
   .Predecessor = {
       {10, 11},
       {0},
       {9, 5},
       {1, 7},
       {10, 11},
       {1, 7},
       {8, 3, 4},
       {9, 5},
       {2, 6},
       {10, 11},
       {},
       {0},
       {9, 5},
       {8, 3, 4}
   },
   .Initial = {10} ,
   .Final = {12, 13} ,
   .Label = {
       {TFD::DontC, TFD::False, TFD::False},
       {TFD::DontC, TFD::True, TFD::True},
       {TFD::True, TFD::True, TFD::DontC},
       {TFD::True, TFD::False, TFD::DontC},
       {TFD::True, TFD::False, TFD::True},
       {TFD::False, TFD::False, TFD::DontC},
       {TFD::DontC, TFD::True, TFD::DontC},
       {TFD::False, TFD::True, TFD::DontC},
       {TFD::DontC, TFD::False, TFD::DontC},
       {TFD::False, TFD::False, TFD::True},
       {TFD::DontC, TFD::True, TFD::DontC},
       {TFD::DontC, TFD::True, TFD::False},
       {TFD::True, TFD::True, TFD::DontC},
       {TFD::DontC, TFD::True, TFD::DontC}
   }
};
}
