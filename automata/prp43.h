namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('b>a+1', 'sing', 'a>b+1', 'a>=0 and b>=0', 't<=T', 't=0')
    Automaton Aut43 = {
        .Predecessor = {
            {10, 12},
            {6},
            {16, 7},
            {17, 20},
            {10, 12},
            {9, 18},
            {1, 15},
            {4, 14},
            {17, 20},
            {11, 13},
            {3, 5},
            {17, 20},
            {4, 14},
            {1, 15},
            {9, 18},
            {},
            {0, 2, 8, 19, 21},
            {11, 13},
            {3, 5},
            {9, 18},
            {6},
            {1, 15},
            {0, 2, 8, 19, 21},
            {4, 14}},
        .Initial = {15},
        .Final = {22, 23},
        .Label = {{TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::DontC, TFD::True}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}}};
}
