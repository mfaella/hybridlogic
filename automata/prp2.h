namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('a>=0 and b>=0', 'sing', 't<=T', 't=0')
    Automaton Aut2 = {
        .Predecessor = {
            {2},
            {2},
            {0, 6},
            {1, 4},
            {3, 5},
            {0, 6},
            {},
            {2},
            {3, 5}},
        .Initial = {6},
        .Final = {8, 7},
        .Label = {{TFD::True, TFD::True, TFD::True, TFD::DontC},
                 {TFD::False, TFD::True, TFD::True, TFD::DontC},
                 {TFD::True, TFD::False, TFD::True, TFD::DontC},
                 {TFD::DontC, TFD::False, TFD::True, TFD::DontC},
                 {TFD::DontC, TFD::True, TFD::True, TFD::DontC},
                {TFD::False, TFD::False, TFD::True, TFD::DontC},
                {TFD::DontC, TFD::True, TFD::DontC, TFD::True},
                {TFD::False, TFD::True, TFD::True, TFD::DontC},
                {TFD::DontC, TFD::True, TFD::True, TFD::DontC}}};
}
