namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('a>b+1', 'sing', 'b>a+1', 'a>=0 and b>=0', 't<=T', 't=0')
    Automaton Aut42 = {
        .Predecessor = {
            {8, 10},
            {6, 14},
            {13, 5},
            {9, 11},
            {8, 10},
            {16, 4},
            {15},
            {6, 14},
            {16, 4},
            {},
            {3, 7},
            {15},
            {9, 11},
            {0, 1, 2, 12},
            {3, 7},
            {9, 11},
            {6, 14},
            {0, 1, 2, 12},
            {16, 4}},
        .Initial = {9},
        .Final = {17, 18},
        .Label = {{TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::DontC, TFD::True}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}}};
}
