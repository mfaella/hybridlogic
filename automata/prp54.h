namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('a>b', 'sing', 'b>a', 't<=T', 'a>=0 and b>=0', 't=0')
    Automaton Aut54 = {
        .Predecessor = {
            {4},
            {13, 7},
            {0, 8},
            {12, 14},
            {0, 8},
            {2, 10, 15, 20, 21, 23},
            {16, 24},
            {1, 19},
            {},
            {3, 6},
            {9, 25},
            {12, 14},
            {17, 26},
            {0, 8},
            {3, 6},
            {12, 14},
            {17, 26},
            {1, 19},
            {11, 22},
            {4},
            {16, 24},
            {18, 5},
            {9, 25},
            {1, 19},
            {13, 7},
            {11, 22},
            {16, 24},
            {11, 22},
            {2, 10, 15, 20, 21, 23}},
        .Initial = {8},
        .Final = {27, 28},
        .Label = {{TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::DontC, TFD::True}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}}};
}
