namespace Automata {
    using TFD = Automaton::TFD;

    // Atomic props: ('t=T', 'sing', 'a>=0 and b>=0', 't=0')
    Automaton Aut1 = {
       .Predecessor = {
           {},
           {0, 6},
           {3, 5},
           {4},
           {0, 6},
           {1, 2},
           {4},
           {4},
           {1, 2}
       },
       .Initial = {0} ,
       .Final = {8, 7} ,
       .Label = {
           {TFD::DontC, TFD::True, TFD::DontC, TFD::True},
           {TFD::True, TFD::False, TFD::True, TFD::DontC},
           {TFD::DontC, TFD::False, TFD::True, TFD::DontC},
           {TFD::True, TFD::True, TFD::True, TFD::DontC},
           {TFD::False, TFD::False, TFD::True, TFD::DontC},
           {TFD::DontC, TFD::True, TFD::True, TFD::DontC},
           {TFD::False, TFD::True, TFD::True, TFD::DontC},
           {TFD::True, TFD::True, TFD::True, TFD::DontC},
           {TFD::DontC, TFD::True, TFD::True, TFD::DontC}
       }
    };
}
