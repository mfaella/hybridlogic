namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('b>a', 'sing', 'a>b', 't<=T', 'a>=0 and b>=0', 't=0')
    Automaton Aut51 = {
        .Predecessor = {
            {2},
            {6, 7},
            {6, 7},
            {0, 8},
            {0, 8},
            {10, 11},
            {},
            {2},
            {1, 4},
            {6, 7},
            {1, 4},
            {9, 3, 5},
            {1, 4},
            {9, 3, 5}},
        .Initial = {6},
        .Final = {12, 13},
        .Label = {{TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::DontC, TFD::True}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::True, TFD::DontC}}};
}
