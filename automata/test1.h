namespace Automata {
  using TFD = Automaton::TFD;

  Automaton A1 = {
    .Predecessor = { {0}, {0}, {1} },
    .Initial = {0},
    .Final = {2},
    .Label = { {TFD::True, TFD::True, TFD::DontC},
	       {TFD::True, TFD::DontC, TFD::False},
	       {TFD::False, TFD::False, TFD::False} }
  };

}
