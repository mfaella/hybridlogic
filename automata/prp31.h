namespace Automata
{
    using TFD = Automaton::TFD;

    // Atomic props: ('b>a', 'sing', 'a>b', 'a>=0 and b>=0', 't=0')
    Automaton Aut31 = {
        .Predecessor = {
            {11, 6},
            {0, 4},
            {11, 6},
            {0, 4},
            {3, 7},
            {8, 9, 2},
            {10},
            {10},
            {3, 7},
            {1, 5},
            {11, 6},
            {},
            {0, 4},
            {8, 9, 2}},
        .Initial = {11},
        .Final = {12, 13},
        .Label = {{TFD::False, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::True, TFD::True, TFD::DontC}, {TFD::False, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::False, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::True, TFD::True, TFD::DontC}, {TFD::True, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::False, TFD::False, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::DontC, TFD::True}, {TFD::True, TFD::True, TFD::DontC, TFD::True, TFD::DontC}, {TFD::DontC, TFD::True, TFD::DontC, TFD::True, TFD::DontC}}};
}
