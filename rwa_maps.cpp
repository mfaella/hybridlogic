#include <ext/hash_map>
#include "basics.h"
#include "rwa_map_definition.h"
#include "pairwise_reduce.h"

double num=0;
double num_check_borders=0;


// For debugging
void print_adj_list(list_nnc_type &l) {
  for (list_nnc_type::iterator it=l.begin(); it!=l.end(); it++) {
    cout << **it;
  }
}

// P /\ PREFLOW(B) CON B={b | (P,b) app extadj}
static PPS cut_polyhedra_with_exit_borders(Poly *p_point, Poly &flow_nnc, hash_map_type& extadj)
{
  // Variabile globale conta_reach commentata, per comodità di compilazione.
	//conta_reach++;
	dimension_type dim=flow_nnc.space_dimension();
	PPS b_ps(dim,EMPTY), flow_ps(flow_nnc), p_ps(*p_point);

	// Cut polyhedron with exit borders
	// Collect the exit borders
	for (list_nnc_type::iterator it_list=extadj[p_point].begin(); it_list!=extadj[p_point].end(); it_list++)
		b_ps.add_disjunct(**it_list);


	// Compute pre-flow of border set
	b_ps.time_elapse_assign(flow_ps);
	fast_pairwise_reduce(b_ps);
	p_ps.intersection_assign(b_ps);
	fast_pairwise_reduce(p_ps);
	return p_ps;
}



// INIZIALIZZA LE HASH MAP INTADJ E EXATDJ E LA CODA QUEQUE
// RISKY SONO NOTV NOT OVER SONO QUELLI DI W
static void init_maps(PPS &risky_ps, PPS &notover_ps, Poly flow_nnc, hash_map_type& intadj, hash_map_type& extadj,
		list_nnc_type& queue)
{
	dimension_type dim=risky_ps.space_dimension();
	hash_map_type::iterator it1_intadj, it2_intadj;
	PS::iterator it_risky, it_notover;
	Poly *p1_point, *p2_point, p1close_nnc(dim), pprime_nnc(dim), b_nnc(dim), *b_point, elapse_nnc(dim);
	list_nnc_type list_nnc;
	bool first_ext;

	// INSERISCI TUTTI GLI ELEMENTI DI NOT_V NELLA HASH MAP INTADJ
	for (it_risky=risky_ps.begin(); it_risky!=risky_ps.end(); it_risky++)
    {
		p1_point = new Poly(it_risky->pointset());
		intadj[p1_point]=list_nnc;
    }

	// PER OGNI P1 IN INTADJ
	for (it1_intadj=intadj.begin(); it1_intadj!=intadj.end(); it1_intadj++)
    {
		p1_point = it1_intadj->first;
		p1close_nnc=*p1_point;
		p1close_nnc.topological_closure_assign();

		//PER OGNI COPPIA P2<>P1 in INTADJ
		it2_intadj=it1_intadj;
		it2_intadj++;
		while (it2_intadj!=intadj.end())
		{
			p2_point = it2_intadj->first;
			b_nnc = simple_border_opt(*p1_point, p1close_nnc, *p2_point);
			num++;

			// SE IL BNDRY NON E' VUOTO, ALLORA P1 E P2 SONO ADIACENTI
			if (!b_nnc.is_empty())
			{
				intadj[p1_point].push_back(p2_point);
				intadj[p2_point].push_back(p1_point);
			}
			++it2_intadj;
		} // ENDFOR P2

		// PER OGNI P' IN W
		first_ext=true;
		for (it_notover=notover_ps.begin(); it_notover!=notover_ps.end(); it_notover++)
		{
			pprime_nnc = it_notover->pointset();
			b_nnc = exit_border_opt(*p1_point, p1close_nnc, pprime_nnc, flow_nnc);
			num++;

			// SE ENTRY NON E' VUOTO, INSERISCI P1 ED ENTRY in extadj
			if (!b_nnc.is_empty())
			{
				if (first_ext)
				{
					extadj[p1_point]=list_nnc;
					queue.push_back(p1_point);
					first_ext=false;
				}
				b_point = new Poly(b_nnc);
				extadj[p1_point].push_back(b_point);
			}
		} // ENDFOR P'
    } // ENDFOR P1
}

//AGGIORNA LE ADIACENZE INTERNE SOLO TRA POLIEDRI DI PNEW . VIENE CHIAMATO QUANDO IL P ORIGINALE NON HA ADIACENTI
// Updates internal adjacencies among pieces of "Pnew".
// Called when the original polyhedron being cut has no adjacents.
static void update_int(PPS &pnew_ps, hash_map_type &intadj)
{
	list_nnc_type list_nnc, mylist_nnc;
	list_nnc_type::iterator it1_list, it2_list;
	Poly *q1_point, *q2_point;
	PS::iterator it_pnew;

	//INSERISCE TUTTI I POLIEDRI DI PNEW IN INTADJ
	//PER OGNI Q1 IN PNEW
	for (it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++)
    {
		q1_point = new Poly(it_pnew->pointset());
		// INIZIALIZZA LA LISTA DI ADIACENZA DI Q1
		intadj[q1_point]=list_nnc;
		mylist_nnc.push_back(q1_point);
	}

	// PER OGNI Q1 IN PNEW
	for (it1_list=mylist_nnc.begin(); it1_list!=mylist_nnc.end(); it1_list++)
	{
		q1_point = *it1_list;
	    Poly q1close_nnc(*q1_point);
		q1close_nnc.topological_closure_assign();

		// PER OGNI COPPIA Q1<>Q2 IN PNEW
		it2_list=it1_list;
		it2_list++;
		while (it2_list!=mylist_nnc.end())
		{
			q2_point = *it2_list;
			//CALCOLA BNDRY(Q1,Q2)
			Poly b_nnc(simple_border_opt(*q1_point, q1close_nnc, *q2_point));
			num++;

			// SE Q1 E Q2 SONO ADIACENTI, INSERISCI Q1 E Q2 IN INTADJ
			if (!b_nnc.is_empty())
			{
				intadj[q1_point].push_back(q2_point);
				intadj[q2_point].push_back(q1_point);
			}
			it2_list++;
		} // ENDFOR Q2
	} // ENDFOR Q1
}

// PRIMO PASSO DELL'AGGIORNAMENTO DELLE ADIACENZE INTERNE DI P2 (ADIACENTE A P): CONTROLLA LE ADIACENZE TRA(P2, Pnew) E (Pnew, Pnew)
static void update_int_first(list_nnc_type &mylist_nnc, Poly *p2_point, hash_map_type &intadj)
{
	list_nnc_type::iterator it1_list, it2_list;
	list_nnc_type list_nnc;
	Poly *q1_point, *q2_point;
	PS:: iterator it_pnew;

	// PER OGNI Q1 IN MYLIST (PER OGNI Q1 IN PNEW)
	for (it1_list=mylist_nnc.begin(); it1_list!=mylist_nnc.end(); it1_list++)
	{
		q1_point = *it1_list;
		Poly q1close_nnc(*q1_point);
		q1close_nnc.topological_closure_assign();

		Poly b_nnc (simple_border_opt(*q1_point, q1close_nnc, *p2_point));
		num++;

		//SE Q1 E P2 SONO ADIACENTI, INSERISCI Q1 E Q2 IN INTADJ
		if (!b_nnc.is_empty())
		{
			intadj[q1_point].push_back(p2_point);
			intadj[p2_point].push_back(q1_point);
		}

		//PER OGNI Q2 DIVERSO DA Q1 IN PNEW
		it2_list=it1_list;
		it2_list++;
		while (it2_list!=mylist_nnc.end())
		{
			q2_point = *it2_list;
			b_nnc = simple_border_opt(*q1_point, q1close_nnc, *q2_point);
			num++;

			//SE Q1 E Q2 SONO ADIACENTI INSERISCI Q1 E Q2 IN INTADJ
			if (!b_nnc.is_empty())
			{
				intadj[q1_point].push_back(q2_point);
				intadj[q2_point].push_back(q1_point);
			}
			++it2_list;
		} // ENDFOR Q2
	} // ENDFOR Q1
}
//SECONDO PASSO DELL'AGGIORNAMENTO DELLE ADIACENZE INTERNE DI P2 ( ADIACENTE A P): CONTROLLA LE ADIACENZE SOLO TRA (P2,PNEW) SENZA CONTOLLARE ANCHE QUELLE TRA (PNEW,PNEW)
static void update_int_succ(list_nnc_type &mylist_nnc, Poly *p2_point, hash_map_type &intadj)
{
	Poly *q_point;
	list_nnc_type:: iterator it_list;

	// PER OGNI Q1 IN MYLIST (PER OGNI Q1 IN PNEW)
	for (it_list=mylist_nnc.begin(); it_list!=mylist_nnc.end(); it_list++)
    {
		q_point = *it_list;
		Poly qclose_nnc(*q_point);
		qclose_nnc.topological_closure_assign();

		Poly b_nnc(simple_border_opt(*q_point, qclose_nnc, *p2_point));
		num++;
		// SE P2 E Q1 SONO ADIACENTI INSERISCILI IN INTADJ
		if (!b_nnc.is_empty())
		{
			intadj[q_point].push_back(p2_point);
			intadj[p2_point].push_back(q_point);
		}
	} // ENDFOR Pnew
}

//AGGIORNA LE ADIACENZE ESTERNE DI P2 (ADIACENTE ALL'ORIGINALE P) CONTROLLANDO I BNDRY TRA P2 E IL CUT CALCOLATO DA P
static void update_external(PPS &cut_ps, Poly *p2_point, Poly &flow_nnc, hash_map_type &extadj, list_nnc_type &queue)
{
	dimension_type dim = flow_nnc.space_dimension();
	list_nnc_type list_nnc;
	Poly q1_nnc(dim), *b_point, p2close_nnc(*p2_point);
	PS:: iterator it_cut;

	p2close_nnc.topological_closure_assign();

	//PER OGNI Q1 IN CUT
	for (it_cut=cut_ps.begin(); it_cut!=cut_ps.end(); it_cut++)
    {
		q1_nnc = it_cut->pointset();
		Poly b_nnc(exit_border_opt(*p2_point, p2close_nnc, q1_nnc, flow_nnc));
		num++;
		//SE LA ENTRY REGION CON P2 NON È VUOTA AGGIORNA L'ADIACENZE ESTERNE E METTE P2 NELLA CODA
		if (!b_nnc.is_empty())
		{
			b_point = new Poly(b_nnc);
			if (extadj.find(p2_point)==extadj.end())
				extadj[p2_point]=list_nnc;

			extadj[p2_point].push_back(b_point);

			bool find=false;

			// Scan queue
			list_nnc_type::iterator it_queue=queue.begin();
			while (it_queue!=queue.end() && (!find))
			{
				if (*it_queue==p2_point)
					find=true;
				it_queue++;
			}
			// INSERISCI P2 NELLA QUEQUE SOLO SE NON E' GIA PRESENTE
			if (!find)
				queue.push_back(p2_point);
		}
	} // ENDFOR cut
}

//AGGIORNAMENTO DELLE ADIACENZE INTERNE E ESTERNE
static void update_int_ext(PPS &pnew_ps, PPS &cut_ps, Poly *p1_point, Poly &flow_nnc, hash_map_type &intadj, hash_map_type &extadj, list_nnc_type &queue)
{
	bool first=true;
	Poly *p2_point,*q_point;
	list_nnc_type mylist_nnc, list_nnc;

	//INSERISCE TUTTI I POLIEDRI DI PNEW IN INTADJ E IN MYLIST
	for (PS::iterator it_pnew=pnew_ps.begin(); it_pnew!=pnew_ps.end(); it_pnew++)
    {
		q_point=new Poly(it_pnew->pointset());
		intadj[q_point]=list_nnc;
		mylist_nnc.push_back(q_point);
    }
	// PER OGNI P2 ADIACENTE A P1
	for (list_nnc_type::iterator it_list=intadj[p1_point].begin(); it_list!=intadj[p1_point].end(); it_list++)
    {
		p2_point=*it_list;

		//RIMUOVI P1 DALLA LISTA DELLE ADIACENZE DI P2(PERCHÈ ADESSO P1 STARA IN W??)
		intadj[p2_point].remove(p1_point);
		// First step of update AGGIORNA LE ADIACENZE DI P2 CONTROLLANDO ANCHE QUELLE TRA POLIEDRI DI PNEW
		if (first)
		{
			update_int_first(mylist_nnc, p2_point, intadj);
			first=false;
		}
		else // Second step AGGIORNA LE ADIACENZE DI P2 SENZA CONTROLLARE PIU QUELLE TRA POLIEDRI DI PNEW
			update_int_succ(mylist_nnc, p2_point, intadj);

		update_external(cut_ps, p2_point, flow_nnc, extadj, queue);
	}
}


/*
LOOP PRINCIPALE
*/
static std::pair<PS, PS> refine_maps(PPS &U_ps, Poly flow_nnc, hash_map_type& intadj, hash_map_type& extadj,
		list_nnc_type& queue)
{
	dimension_type dim=flow_nnc.space_dimension();
	Poly *p_point, p_nnc(dim), b_nnc(dim), pprime_nnc(dim), pprime_close_nnc(dim);
	PPS cut_ps(dim), totalcut_ps(dim,EMPTY);
	PS::iterator it_under;
	PPS W(U_ps);
	hash_map_type::iterator it_int;
	list_nnc_type::iterator it_list;

	// ANALIZZA QUEUE
	while (!queue.empty())
    {
		// ESTRAI IL POLIEDRO P DA QUEUE
		p_point=queue.front();
		// ELIMINA P DA QUEUE
		queue.pop_front();
		p_nnc = *p_point;

	    // CALCOLA CUT
		cut_ps = cut_polyhedra_with_exit_borders(p_point, flow_nnc, extadj);
		// SE CUT NON E' VUOTO AGGIORNA LE MAP
		if (!cut_ps.empty())
		{
			W.upper_bound_assign(cut_ps);
			PPS pnew_ps(dim,EMPTY);
			pnew_ps.add_disjunct(p_nnc);
			// CALCOLA P \ CUT CHE SONO I PUNTI DI P CHE NON SONO AGGIUNTI AL RISULTATO
			pnew_ps.difference_assign(cut_ps);
			fast_pairwise_reduce(pnew_ps);

			// CONTROLLA SE P HA ADIACENTI INTERNI
			if (!intadj[p_point].empty())
				// AGGIORNA ADIACENZE INTERNE E ESTERNE ( SE P HA ADIACENTI)
				update_int_ext(pnew_ps, cut_ps, p_point, flow_nnc, intadj, extadj, queue);
			else
				// AGGIORNA ADIACENZE  INTERNE (PERCHE P NON HA ADIACENTI)
				update_int(pnew_ps, intadj);

			// RIMUOVI P DA INTADJ
			intadj.erase(p_point);

			// FREE MEMORY: Delete all external objects
			for (it_list=extadj[p_point].begin(); it_list!=extadj[p_point].end(); it_list++)
				delete (*it_list);
			extadj.erase(p_point);
			// Delete original P
			delete p_point;
		} // END IF
    } // END WHILE

	fast_pairwise_reduce(W);

	for (it_int=intadj.begin(); it_int!=intadj.end(); it_int++)
		totalcut_ps.add_disjunct(*(it_int->first));
	fast_pairwise_reduce(totalcut_ps);

    return std::make_pair(W, totalcut_ps);
}


/* Returns a partition of "not V" into a pair of polyhedra (A, B):
   - A is the set of points that may reach U while avoiding V
   - B contains the other points of not V
   So, A \/ B = not V

   This version admits a finite number of sharp corners in each trajectory.
   It uses adjacency maps.
*/
std::pair<PPS, PPS> rwa_maps(PPS U_ps, PPS V_ps, Poly preflow)
{
  dimension_type dim=U_ps.space_dimension();
  hash_map_type intadj, extadj;
  list_nnc_type queue;
  PPS not_V_ps(dim, UNIVERSE);
  V_ps.add_space_dimensions_and_embed(dim-V_ps.space_dimension());
  
  // CALCOLA NOT_V
  not_V_ps.difference_assign(V_ps);
  not_V_ps.difference_assign(U_ps);
  fast_pairwise_reduce(not_V_ps);
  
  init_maps(not_V_ps, U_ps, preflow, intadj, extadj, queue);
  return refine_maps(U_ps, preflow, intadj, extadj, queue);
}
