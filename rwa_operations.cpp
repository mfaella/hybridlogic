#include "basics.h"
#include "rwa_operations.h"
#include "rwa_map_definition.h"
#include "pairwise_reduce.h"



// P /\ PREFLOW(B) CON B={b | (P,b) app extadj}
PS cut_polyhedra_with_exit_borders(Poly *p_point, Poly &flow_nnc, hash_map_type& extadj)
{
  // Variabile globale conta_reach commentata, per comodità di compilazione.
	//conta_reach++;
	dimension_type dim=flow_nnc.space_dimension();
	PS b_ps(dim,EMPTY), flow_ps(flow_nnc), p_ps(*p_point);

	// Cut polyhedron with exit borders
	// Collect the exit borders
	for (list_nnc_type::iterator it_list=extadj[p_point].begin(); it_list!=extadj[p_point].end(); it_list++)
		b_ps.add_disjunct(**it_list);


	// Compute pre-flow of border set
	b_ps.time_elapse_assign(flow_ps);
	fast_pairwise_reduce(b_ps);
	p_ps.intersection_assign(b_ps);
	fast_pairwise_reduce(p_ps);
	return p_ps;
}


// // Aggiunge a [b] tutti i poliedri in [a], aggiungendo a ciascuno il generatore [gen].
// static void generator_time_elapse(PS &a, PS &b,
// 				 const Parma_Polyhedra_Library::Generator &gen)
// {
//   for (PS::iterator p_it = a.begin(); p_it != a.end(); p_it++) {
//     Poly p = p_it->pointset();
//     p.add_generator(gen);
//     b.add_disjunct(p);
//   }
// }



/* Returns the "universal preflow" of [r_original].
   This is guaranteed to be an over-approximation to the set
   of points that must reach [r_original] under flow [flow].

   Notice: the universal preflow of P is the intersection,
   over the rays r of the flow, of the existential preflow
   of P w.r.t. r.
 */
/*
PS get_univ_preflow(PS g_ps,const  Poly &flow)
{
	int dim = g_ps.space_dimension();
	Poly origin_flow = get_origin(dim);
	// Apply existential preflow over origin
	origin_flow.time_elapse_assign(flow);
	// Get generators of origin_flow
	Generator_System gs = origin_flow.minimized_generators();

	//  PS g_closed_ps(dim, EMPTY);
	PS result(dim);      // initially TRUE
	PS::iterator p_it;

	// Compute the closure of g
	for (p_it = g_ps.begin(); p_it!=g_ps.end(); p_it++) {
		Poly &p = (Poly &)p_it->pointset();
		p.topological_closure_assign();
	}
	PS &g_closed_ps = g_ps; // just an alias

	for (Generator_System::const_iterator gen=gs.begin(); gen!=gs.end(); ++gen) {
		PS temp(dim, EMPTY);
		if (gen->is_ray() | gen->is_line()) { // a ray
			generator_time_elapse(g_closed_ps, temp, *gen);
			result.intersection_assign(temp);
		} else { // a vertex or a closure point
			int not_zero = 0; // WORKAROUND TO SIMULATE gen->is_zero() !!!!
			//        std::cout << "ADDING A POINT" << endl;
			//      if (gen->is_point()) {
			//        cout << "Point g1: " << g1 << endl;
			//        Linear_Expression e;
			for (int i = gen->space_dimension() - 1; i >= 0; i--)
				if (gen->coefficient(Variable(i)) != 0)
					not_zero+=1; // END WORKAROUND!!!!
			//      }
			//      else {
			//        not_zero = 1;
			//    }
			if (not_zero) {
				Linear_Expression le(*gen);
				//    if (!le.is_zero()) {
				generator_time_elapse(g_closed_ps, temp, ray(le));
				result.intersection_assign(temp);
				//    }
			}
		}
	}
	return result;
}
*/



// PS get_univ_preflow(const PS &g_ps, const  Poly &flow)
// {
//   int dim = g_ps.space_dimension();
//   Poly origin_flow = get_origin(dim);
//   // Apply existential preflow over origin
//   origin_flow.time_elapse_assign(flow);
//   // Get generators of origin_flow
//   Generator_System gs = origin_flow.minimized_generators();

//   //  PS g_closed_ps(dim, EMPTY);
//   PS result(dim);      // initially TRUE
//   PS::const_iterator p_it;

//   PS g_closed_ps(dim, EMPTY);
//   // Compute the closure of g
//   for (p_it = g_ps.begin(); p_it!=g_ps.end(); p_it++) {
//     //Poly &p = (Poly &)p_it->pointset();
//     Poly p = p_it->pointset();
//     Poly p2(p);
//     p2.topological_closure_assign();
//     g_closed_ps.add_disjunct(p2);
//   }

//   for (Generator_System::const_iterator gen=gs.begin(); gen!=gs.end(); ++gen) {
//     PS temp(dim, EMPTY);
//     if (gen->is_ray() | gen->is_line()) { // a ray
//       //        std::cout << "ADDING A RAY" << endl;
//       generator_time_elapse(g_closed_ps, temp, *gen);
//       result.intersection_assign(temp);
//     } else { // a vertex or a closure point
//       int not_zero = 0; // WORKAROUND TO SIMULATE gen->is_zero() !!!!
// 			//        std::cout << "ADDING A POINT" << endl;
// 			//      if (gen->is_point()) {
// 			//        cout << "Point g1: " << g1 << endl;
// 			//        Linear_Expression e;
//       for (int i = gen->space_dimension() - 1; i >= 0; i--)
// 	if (gen->coefficient(Variable(i)) != 0)
// 	  not_zero+=1; // END WORKAROUND!!!!
//       //      }
//       //      else {
//       //        not_zero = 1;
//       //    }
//       if (not_zero) {
// 	Linear_Expression le(gen->expression());
// 	//      std::cout << "PPP" << endl;
// 	generator_time_elapse(g_closed_ps, temp, ray(le));
// 	result.intersection_assign(temp);
// 	//      std::cout << "QQQ" << endl;
// 	//      std::cout << "UNIVERSAL: " << result << endl;
//       }
//     }
//   }
//   fast_pairwise_reduce(result);
//   return result;
// }


// // RIMUOVE LA PARTE UNBOUNDED DI [a_ps] RISPETTO [postflow_nnc]
// void remove_unbounded(PS &a_ps, Poly postflow_nnc, Poly preflow_nnc)
// {
// 	dimension_type dim = a_ps.space_dimension();
// 	PS::iterator it;
// 	Poly p_nnc(dim), ptime_nnc(dim);

// 	list_powerset_type to_be_added;

// 	postflow_nnc.topological_closure_assign();
// 	// For each convex P in [a_ps]
// 	for (it = a_ps.begin(); it != a_ps.end();)
//     {
// 		p_nnc = it->pointset();


// 		// If P is not bounded
// 		if (!is_bounded(p_nnc, postflow_nnc))
// 		{


// 			// cut the positive pre-flow of P from P
// 			ptime_nnc = positive_time_elapse(p_nnc, preflow_nnc);
// 			PS p_ps(p_nnc), ptime_ps(ptime_nnc);
// 			p_ps.difference_assign(ptime_ps);
// 			it = a_ps.drop_disjunct(it);
// 			to_be_added.push_back(p_ps);
// 		}
// 		else
// 			it++;
// 	}

// 	for (list_powerset_type::iterator l_it=to_be_added.begin();l_it!=to_be_added.end();l_it++)
// 	  add_powerset(a_ps, *l_it);

// 	return;
// }

// // RITORNA TRUE SSE [p_nnc] E' BOUNDED RISPETTO [flow_nnc].
// bool is_bounded(const Poly &p_nnc, const Poly &flow_nnc)
// {
//   dimension_type dim=p_nnc.space_dimension();
//   Constraint_System cs;
//   Generator_System gs;

//   const Poly &origin_nnc = get_origin(dim);

//   // If Origin belongs to the flow then P is not bounded
//   if (flow_nnc.contains(origin_nnc))
//     return false;

//   // Build characteristic cone of P, i.e.,
//   // add the rays of P to the Origin
//   Poly cone_nnc(origin_nnc);
//   gs=p_nnc.generators();

//   for (Generator_System::const_iterator gen=gs.begin(); gen!=gs.end(); ++gen)
//     if (gen->is_ray()) { // add ray [gen] to Origin
//       // cout << "Adding ray " << *gen << endl;
//       cone_nnc.add_generator(*gen);
//     } else if (gen->is_line()) {


//       //Linear_Expression le(*gen);
//       // La precedente riga di codice commentata è stata sostituita dalla riga
//       // successiva a questo commento con il nuovo metodo di PPL.
//       Linear_Expression le(gen->expression());
//       // Add first ray of the line
//       cone_nnc.add_generator(ray(le));
//       // Add second ray of the line
//       cone_nnc.add_generator(ray(-le));
//     }
//   cone_nnc.intersection_assign(flow_nnc);

//   if (cone_nnc.is_empty())
//     return true;

//   return false;
// }

// //RITORNA IL POLIEDRO ORIGINE DI DIMENSIONE DIM
// Poly get_origin(int dim)
// {
//   Constraint_System cs;

//   for (int i=0; i<dim; i++) {
//     Variable var(i);
//     cs.insert(var==0);
//   }

//   Poly origin(cs);
//   return origin;
// }




// // Tries to simplify each convex polyhedron in [ps].
// void simplify(PS &ps)
// {

//   PS::iterator it;
//   // For each convex P in [ps]
//   for (it = ps.begin(); it != ps.end(); it++)
// 	  it->pointset().minimized_constraints();
// }
