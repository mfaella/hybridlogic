#include <exception>
#include <string>
#include <vector>

#include "ltlf_mc.hpp"

constexpr bool DEBUG = false;

static int recursion = 0;

static std::vector<int> statecounters;

PPS rwa_ltlf(const Automaton & Aut, int s, const Poly & P, const Poly & X, const Poly & preflow, State2PPS & ResidualDenot, int sing) {

  statecounters[s]++;

  recursion++;

  if (DEBUG) {
    cout << endl
      << ' ' << std::string(recursion, '\t') << s << '\t';
  }

  // int statenum = Aut.Label.size();
  // for (int s = 0; s < statenum; ++s) {
  //  cout << s << ": " << ResidualDenot[s] << endl;
  //}

  // Base case
  if (Aut.Initial.find(s) != Aut.Initial.end()) {
    if (DEBUG) {
      cout << "(initial) adding: " << X;
      recursion--;
    }
    return PPS(X);
  }

  // Line 3
  State2PPS ResidualDenotPrime;
  if (Aut.Label[s][sing] == Automaton::TFD::DontC) {
    throw runtime_error("ERROR: Don't care on Sing for state " + to_string(s) + "!");
  }
  else if (Aut.Label[s][sing] == Automaton::TFD::False) { // s is Open
    ResidualDenotPrime = ResidualDenot;
    for (PPS::iterator PatchItr = ResidualDenotPrime[s].begin();
      PatchItr != ResidualDenotPrime[s].end();
      PatchItr++) {
      if (PatchItr->pointset() == P) {
        ResidualDenotPrime[s].drop_disjunct(PatchItr);
        break;
      }
    }
  }

  // Main loop
  PPS Result(P.space_dimension(), EMPTY);
  for (int sprime : Aut.Predecessor[s]) {
    if (DEBUG) {
      cout << endl
        << std::string(recursion, '\t') << "   <- " << sprime;
    }
    if (Aut.Label[s][sing] == Automaton::TFD::True) {
      if (Aut.Label[sprime][sing] != Automaton::TFD::False)
        throw runtime_error("Singular and open states do not alternate 1: " + to_string(sprime) + " -> " + to_string(s));
      // s is Sing, so s' is Open!
      ReachPairs RP = reach_plus(ResidualDenot[sprime], X, preflow);
      if (DEBUG) {
        if (RP.size() == 0)
          cout << " (empty) ";
      }
      for (auto PairQY : RP) {
        add_powerset(Result, rwa_ltlf(Aut, sprime, PairQY.first, PairQY.second, preflow, ResidualDenot, sing));
      }
    }
    else {
      if (Aut.Label[sprime][sing] != Automaton::TFD::True)
        throw runtime_error("Singular and open states do not alternate 2: " + to_string(sprime) + " -> " + to_string(s));
      // s is Open, so s' is Sing!
      ReachPairs RP = reach0(ResidualDenot[sprime], X, preflow);
      if (DEBUG) {
        if (RP.size() == 0)
          cout << " (empty) ";
      }
      for (auto PairQY : RP) {
        add_powerset(Result, rwa_ltlf(Aut, sprime, PairQY.first, PairQY.second, preflow, ResidualDenotPrime, sing));
      }
    }
  }
  if (DEBUG)
    recursion--;
  return Result;
}

PPS ltlf_mc(const Automaton & Aut, const PolySys & PSys, int sing) {

  statecounters = std::vector<int>(Aut.Label.size(), 0);

  int statenum = Aut.Predecessor.size();
  int apnum = PSys.AP2PPS.size();
  int dimension = PSys.dimension;

  vector<PPS> NotAP2PPS(apnum);
  for (int ap = 0; ap < apnum; ++ap) {
    if (ap != sing) {
      NotAP2PPS[ap] = PPS(dimension);
      NotAP2PPS[ap].difference_assign(PSys.AP2PPS[ap]);
    }
  }

  if (DEBUG) {
    cout << "Residual initialisation" << endl;
  }

  // Residual initialisation
  State2PPS ResidualDenot(statenum);
  for (int s = 0; s < statenum; ++s) {
    if (DEBUG) {
      cout << endl;
    }
    ResidualDenot[s] = PPS(dimension);
    for (int ap = 0; ap < apnum; ++ap) {
      if (ap != sing) {
        if (DEBUG) {
          cout << "(s; ap): " << s << "; " << ap << "  " << flush;
        }
        if (Aut.Label[s][ap] == Automaton::TFD::True) {
          ResidualDenot[s].intersection_assign(PSys.AP2PPS[ap]);
        }
        else if (Aut.Label[s][ap] == Automaton::TFD::False) {
          ResidualDenot[s].intersection_assign(NotAP2PPS[ap]);
        }
      }
    }
  }

  if (DEBUG) {
    cout << "Main loop" << endl;
  }

  PPS Result(dimension, EMPTY);
  for (int s : Aut.Final) {
    for (PPS::iterator PatchItr = ResidualDenot[s].begin();
      PatchItr != ResidualDenot[s].end(); PatchItr++) {
      Poly Patch = PatchItr->pointset();
      PPS contribution = rwa_ltlf(Aut, s, Patch, Patch, PSys.preflow, ResidualDenot, sing);
      if (DEBUG) {
        cout << endl
          << "Added by " << s << ": " << contribution << endl;
      }
      add_powerset(Result, contribution);
    }
  }

  // for (ulong s = 0; s < statecounters.size(); s++) {
  //   cout << s << ": " << statecounters[s] << "; ";
  // }

  // cout << endl << "#Recs: " << recursion << " ";
  // recursion = 0;

  return Result;
}
