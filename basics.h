#ifndef _BASICS_H_
#define _BASICS_H_

#include <ppl.hh>

using namespace std;
using namespace Parma_Polyhedra_Library;
using namespace Parma_Polyhedra_Library::IO_Operators;

typedef NNC_Polyhedron Poly;
typedef Pointset_Powerset<NNC_Polyhedron> PPS;

void insert_all(Constraint_System &sys, const vector<Constraint> &constraints);
Poly make_poly(const vector<Constraint> &constraints, int dim=0);
Poly make_poly(const vector<Generator> &points);
Poly make_poly(const vector<Generator> &points, int dim);


// Funzione che fa le veci della vecchia continuous_set_PPL_Powerset::negate(), serve per negare il PS.
void negate(PPS& to_negate);

//fa l'inverso rispetto all'origine. Utilizzato dall'operatore SORM.
void prepoly_assign(Poly& poly);

/*
    Adds second to first
  */
void add_powerset(PPS &first, const PPS &second);

inline bool are_equal(const PPS &first, const PPS &second)
{
  return first.geometrically_covers(second) && second.geometrically_covers(first);
}


/*
 * affine_hull restituisce l'affine hull del poliedro ph preso in input
 *
 *  INPUT:	ph 	(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare l'affine hull;
 *  OUTPUT:	ahull	(NNC_Polyhedron)	-> affine hull di ph;
 *
 */
Poly affine_hull(const Poly &ph);


/*
 * relative_interior restituisce il relative interior del poliedro preso in input
 *
 *  INPUT:   ph 			(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare
 * 					   	   il relative interior;
 *  OUTPUT:  ph_relative_interior	(NNC_Polyhedron)	-> relative interior di ph;
 *
 */
Poly relative_interior(const Poly &ph);


/*
 * zero_mirror_assign trasforma il poliedro passato nella sua riflessione rispetto l'origine
 *
 * 	INPUT:   ph 	(NNC_Polyhedron)	-> poliedro da riflettere;
 * 	OUTPUT:  ph	(NNC_Polyhedron)	-> riflessione di ph rispetto l'origine;
 *
 */
void zero_mirror_assign(Poly &ph);


/*
 * point_mirror restituisce la riflessione di ph rispetto al punto x,
 * cioé 2*x-y, con y appartenente a ph
 *
 * 	INPUT:	ph 	(NNC_Polyhedron)	-> poliedro da riflettere;
 *		x	(Generator)	-> punto rispetto al quale riflettere;
 * 	OUTPUT:	my_ph 	(NNC_Polyhedron)	-> riflessione di ph rispetto a x
 *
 */
Poly point_mirror(const Poly &ph, const Generator &x);


/*
 * mirror restituisce la riflessione di poly rispetto l'affine hull di set
 *
 *  INPUT:	poly 	 (NNC_Polyhedron)	-> poliedro da riflettere;
 * 		set 	 (NNC_Polyhedron)	-> poliedro rispetto al quale riflettere
 *  OUTPUT:	poly_copy(NNC_Polyhedron)	-> risultato della riflessione di poly
 *
 */
Poly mirror(const Poly &poly, const Poly &set);


/*
 * bndry(ph1,ph2) = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 *
 *   INPUT:   ph1, ph2  (NNC_Polyhedron)
 *   OUTPUT:  output    (NNC_Polyhedron) -> output = (ph1/\cl(ph2))\/(ph2/\cl(ph1))
 *
 */
Poly bndry(const Poly &ph1,const Poly &ph2);


//CALCOLA BNDRY(P1,P2)
Poly simple_border_opt(Poly &poly1_nnc, Poly &poly1close_nnc, Poly &poly2_nnc);

//CALCOLA ENTRY(INTERNAL_POLY_NNC, EXTERNAL_POLY_NNC)
Poly exit_border_opt(Poly &internal_poly_nnc, Poly &internal_polyclose_nnc, Poly &external_poly_nnc, Poly &flow_nnc);

// RITORNA TRUE SSE [p_nnc] E' BOUNDED RISPETTO [flow_nnc].
bool is_bounded(const Poly &p_nnc, const Poly &flow_nnc);
// RIMUOVE LA PARTE UNBOUNDED DI [a_ps] RISPETTO [postflow_nnc]
void remove_unbounded(PPS &a_ps, Poly postflow_nnc, Poly preflow_nnc);
//RITORNA IL POLIEDRO ORIGINE DI DIMENSIONE DIM
Poly get_origin(int dim);

// Tries to simplify each convex polyhedron in [ps].
void simplify(PPS &ps);

#endif
