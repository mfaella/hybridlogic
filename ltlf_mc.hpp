
#include <ppl.hh>
#include <vector>
#include <set>

#include "basics.h"
#include "reach.hpp"

using namespace std;

struct Automaton
{
  // int APNum = 0;
  // int StateNum = 0;
  enum class TFD
  {
    True,
    False,
    DontC
  };
  vector<vector<int>> Predecessor;
  set<int> Initial;
  set<int> Final;
  vector<vector<TFD>> Label;
};

struct PolySys
{
  // int APNum = 0;
  int dimension;
  Poly preflow;
  vector<PPS> AP2PPS;
};

typedef vector<PPS> State2PPS;

PPS rwa_ltlf(const Automaton &Aut, int s, const Poly &P, const Poly &X, const Poly &preflow, State2PPS &ResidualDenot, int sing);

PPS ltlf_mc(const Automaton &Aut, const PolySys &PSys, int sing);
