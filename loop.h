#ifndef _LOOP_H_
#define _LOOP_H_

#include "basics.h"
#include "flow.h"

typedef vector<Poly> Loop;

/* Returns true if each polyhedron in the loop is disjoint from
   and adjacent to the next one. */
bool is_proper_loop(const Loop& loop);

/** Returns the loop where all points in each polyhedron
    can move to the next polyhedron.
*/
Loop one_step(const Loop& l, const Flow& flow);

/** Returns the loop where all points in each polyhedron
    can traverse the whole loop and go back to the original polyhedron.
*/
Loop n_steps(const Loop& l, const Flow& flow);

/* Let P0 be loop[0].
   This function returns the set of points of P0 that
   can traverse the loop and go back to P0. */
Poly traverse_loop(const Loop& loop, const Flow& flow);

ostream& operator<<(ostream& os, const Loop& loop);

ostream& operator<<=(ostream& os, const Loop& loop);

// bool operator==(const Loop& loop1, const Loop& loop2);

#endif
