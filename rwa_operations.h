#ifndef RWA_OPERATIONS_H_
#define RWA_OPERATIONS_H_

#include <ppl.hh>

//#include<ext/hash_map>

// To include hash_map_type, list_nnc_type and NNC_PolyhedronTraits_loc definitions.
#include "rwa_map_definition.h"

using namespace Parma_Polyhedra_Library;
//using namespace __gnu_cxx;
using namespace std;


// BNDRY(P1,P2)
Poly simple_border_opt(Poly &poly1_nnc, Poly &poly1close_nnc, Poly &poly2_nnc);

// ENTRY(INTERNAL_POLY_NNC, EXTERNAL_POLY_NNC)
Poly exit_border_opt(Poly &internal_poly_nnc, Poly &internal_polyclose_nnc, Poly &external_poly_nnc, Poly &flow_nnc);


// P /\ PREFLOW(B) CON B={b | (P,b) app extadj}

PPS cut_polyhedra_with_exit_borders(Poly *p_point, Poly &flow_nnc, hash_map_type& extadj);


/* Returns the "universal preflow" of [r_original].
   This is guaranteed to be an over-approximation to the set
   of points that must reach [r_original] under flow [flow].

   Notice: the universal preflow of P is the intersection,
   over the rays r of the flow, of the existential preflow
   of P w.r.t. r.
 */
PPS get_univ_preflow(const PPS &g_ps, const Poly &flow);

// RITORNA TRUE SSE [p_nnc] E' BOUNDED RISPETTO [flow_nnc].
bool is_bounded(const Poly &p_nnc, const Poly &flow_nnc);
// RIMUOVE LA PARTE UNBOUNDED DI [a_ps] RISPETTO [postflow_nnc]
void remove_unbounded(PPS &a_ps, Poly postflow_nnc, Poly preflow_nnc);
//RITORNA IL POLIEDRO ORIGINE DI DIMENSIONE DIM
Poly get_origin(int dim);


// Tries to simplify each convex polyhedron in [ps].
void simplify(PPS &ps);

#endif /*RWA_OPERATIONS_H_*/
