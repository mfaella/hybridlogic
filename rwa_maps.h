#include "basics.h"

/* Returns a partition of "not V" into a pair of polyhedra (A, B):
   - A is the set of points that may reach U while avoiding V
   - B contains the other points of not V
   So, A \/ B = not V

   This version admits a finite number of sharp corners in each trajectory.
   It uses adjacency maps.
*/

 std::pair<PPS, PPS> rwa_maps(PPS U_ps, PPS V_ps, Poly preflow);


