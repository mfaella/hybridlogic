#include "basics.h"
#include "loop.h"

/** Returns true if successive polyhedra are disjoint and adjacent. */
bool is_proper_loop(const Loop& l) {
  const int n = l.size();
  for (int i=0; i<n; i++) {
    int next = (i+1) % n;
    Poly intersection(l[i]);
    intersection.intersection_assign(l[next]);
    if (!intersection.is_empty())
      return false;
    Poly boundary = bndry(l[i], l[next]);
    if (boundary.is_empty())
      return false;
  }
  return true;
}


/** Returns the set of points of a that can move directly into b. */
Poly reach(const Poly& a, const Poly& b, const Flow& flow) {
  Poly boundary = bndry(a, b);
//   cout << "    > Boundary " << boundary << endl;
  Poly bb(b);
  bb.time_elapse_assign(flow.pre());
  boundary.intersection_assign(bb);
//   cout << "    > Good boundary " << boundary << endl;
  boundary.time_elapse_assign(flow.pre());
  Poly result(a);
  result.intersection_assign(boundary);
  return result;
}

/** Returns the loop where all points in each polyhedron
    can move to the next polyhedron.
*/
Loop one_step(const Loop& l, const Flow& flow) {
  const int n = l.size();
  Loop result;
  for (int i=0; i<n; i++) {
    int next = (i+1) % n;
    Poly p = reach(l[i], l[next], flow);
    result.push_back(p);
  }
  return result;
}


Loop n_steps(const Loop& l, const Flow& flow) {
  const int n = l.size();
  Loop result(l);
  for (int i=0; i<n; i++) {
    cout << "  > Internal Iteration (n_steps) " << i << ":" << endl;
    (cout << "  > Current loop: " <<= result) << endl;
    result = one_step(result, flow);
  }
  return result;
}

/** Returns the points of the first polyhedron that can
    traverse the whole loop and go back to the first polyhedron.
*/
Poly traverse_loop(const Loop& l, const Flow& flow) {
  const int n = l.size();
  Poly previous(l[0]);
  for (int i=n-1; i>=0; i--) {
    previous = reach(l[i], previous, flow);
  }
  return previous;
}


ostream& operator<<(ostream& os, const Loop& loop) {
  for (const Poly& p: loop) {
    os << " { " << p << " }";
  }
  return os;
}

ostream& operator<<=(ostream& os, const Loop& loop) {
  for (const Poly& p: loop) {
    os << " { " << p.minimized_generators() << " }";
  }
  return os;
}
