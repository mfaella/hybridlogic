
#ifndef RWA_MAP_DEFINITION_H_
#define RWA_MAP_DEFINITION_H_

#include <ppl.hh>

// #include "../../io/polyhedra/ppl_polyhedron/extended_ppl.h"
#include <ext/hash_map>
/*
DEFINIZIONI DELLE HASH-MAP UTILIZZATE DALLE VERSIONI DI RWA

*/
using namespace Parma_Polyhedra_Library;
using namespace __gnu_cxx;
using namespace std;

struct NNC_PolyhedronTraits_loc
{
  size_t operator()( const NNC_Polyhedron* that ) const
  {
    return (size_t) that;
  }
  bool operator()( const NNC_Polyhedron* that1, const NNC_Polyhedron* that2 ) const
  {
    return that1 == that2;
  }
};


typedef NNC_Polyhedron Poly;
typedef Pointset_Powerset<NNC_Polyhedron> PS;

// An entry in the external adjacency map
class pair_poly_ps {
 public:
  pair_poly_ps(const Poly &a, const PS &b, const Poly &c){
    this->my_poly=a;
    this->my_powerset=b;
    this->my_cl=c;

    if (a.is_empty()) cout << "Warning: a is empty!" << endl;
    if (b.is_empty()) cout << "Warning: b is empty!" << endl;
    if (c.is_empty()) cout << "Warning: c is empty!" << endl;
  };
  ~pair_poly_ps() { };

  PS &get_PS() {
    return this->my_powerset;
  };
  Poly &get_Poly() {
    return this->my_poly;
  };
  Poly &get_cl() {
    return this->my_cl;
  };
  void set_PS(PS ps) {
    this->my_powerset=ps;
  }
  void set_Poly(Poly p) {
    this->my_poly=p;
  }
 private:
  Poly my_poly;
  PS my_powerset;
  Poly my_cl;
};

typedef list<NNC_Polyhedron*> list_nnc_type;
typedef hash_map<NNC_Polyhedron*, list_nnc_type, NNC_PolyhedronTraits_loc, NNC_PolyhedronTraits_loc> hash_map_type;
typedef list<pair_poly_ps *> list_ps_type;
typedef list<PS> list_powerset_type;
typedef hash_map<NNC_Polyhedron*, list_ps_type, NNC_PolyhedronTraits_loc, NNC_PolyhedronTraits_loc> hash_map_ps_type;
typedef hash_map<NNC_Polyhedron*, unsigned int, NNC_PolyhedronTraits_loc, NNC_PolyhedronTraits_loc> hash_map_u_type;

// A map from a polyhedra pointer to another polyhedra pointer (closure map)
typedef hash_map<NNC_Polyhedron*, NNC_Polyhedron*, NNC_PolyhedronTraits_loc, NNC_PolyhedronTraits_loc> closure_map_t;




namespace continuous {
typedef Parma_Polyhedra_Library::Pointset_Powerset<Parma_Polyhedra_Library::NNC_Polyhedron> PS;
}



#endif /*RWA_MAP_DEFINITION_H_*/
