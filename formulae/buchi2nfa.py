#!/usr/bin/python3

import sys
from hoa.parsers import HOAParser
from hoa.core import HOA
from hoa.ast.label import LabelExpression, LabelAtom
from hoa.ast.boolean_expression import (
    And,
    BinaryOp,
    boolean_op_wrapper,
    FalseFormula,
    Not,
    Or,
    TrueFormula,
    UnaryOp,
)

from dataclasses import dataclass
from typing import Dict, FrozenSet, Optional, Sequence, Tuple, Union

@dataclass(frozen=True, order=True, unsafe_hash=True)
class NFA:
    AP: Sequence[str]
    label: Dict[int, Sequence[int]]
    successors: Dict[int,FrozenSet[int]]
    predecessors: Dict[int,FrozenSet[int]]
    initial: FrozenSet[int]
    accepting: FrozenSet[int]

    
def DecodeLabel(label: LabelExpression, n: int):
    """Decodes a SPOT label into a vector of {True, False, None} values, one for each AP."""
    result = [None] * n
    def DecodeSingleProp(label):
        if isinstance(label, LabelAtom):
            result[label.proposition] = True
        elif isinstance(label, Not):
            atom = label.argument
            if not isinstance(atom, LabelAtom):
                raise Exception("Unsupported label {0}.".format(label))
            result[atom.proposition] = False
        else:
            raise Exception("Unsupported label {0}.".format(label))
                
    def DecodeAnd(andLabel):
        for op in andLabel.operands:
            DecodeSingleProp(op)
        
    if isinstance(label, And):
        DecodeAnd(label)
    else: 
        DecodeSingleProp(label)
    return result


def BuchiToNFA(automaton: HOA):
    """Converts a Buchi automaton corresponding to an LTLf property into an NFA. It assumes that the FIRST atomic proposition of the Buchi automaton is the 'alive' proposition that identifies the relevant prefix."""
    if (automaton.header.acceptance.name != "Buchi"):
        raise Exception("The input automaton does not have the Buchi acceptance condition.")

    accepting_state_id = None
    state_id_to_state = {} 

    # Initialize state_id_to_state and accepting_state_id
    for state in automaton.body.state2edges:
        state_id_to_state[state.index] = state
        if (state.acc_sig is not None): # found an accepting state
            if (accepting_state_id is not None):
                raise Exception("More than one accepting state found.")
            accepting_state_id = state.index

    # Initialize initial_state_id            
    initial_state_id = None
    initial_states = automaton.header.start_states    
    if initial_states is None:
        raise Exception("No initial state found.")
    if len(initial_states) != 1:
        raise Exception("More than one initial state found.")
    initial_states = initial_states.__iter__().__next__()
    if len(initial_states) != 1:
        raise Exception("More than one initial state found.")
    initial_state_id = initial_states.__iter__().__next__()
    #print("Initial state: " + str(initial_state_id))

    n_prop = len(automaton.header.propositions)
    if n_prop == 0:
        raise Exception("No atomic propositions found.")
    if automaton.header.propositions[0] != "alive":
        raise Exception("The first atomic proposition should be called 'alive'.")
    
    new_states = set()
    state_to_labels = {}
    new_state_to_states = {}
    
    for state, edges in automaton.body.state2edges.items():
        # print(state.index)
        # if state.index == accepting_state_id:
        #    continue
        for edge in edges:
            new_state = (state, edge.label)            
            new_states.add(new_state)
            
            dests = edge.state_conj
            if len(dests)>1:
                raise Exception("An edge has more than one destination.")
            dest_id = dests[0]
            dest = state_id_to_state[dest_id]
            #print(dest)
            if state_to_labels.get(state) is None:
                state_to_labels[state] = []
            if new_state_to_states.get(new_state) is None:
                new_state_to_states[new_state] = []
            state_to_labels[state].append(edge.label)
            new_state_to_states[new_state].append(dest)
            # print(edge)

    # Number new states (give them new ids)
    state_counter = 0
    new_state_to_id = {}
    label = {}

    for s in new_states:
        # print("{0}: {1}".format(state_counter, s))
        new_state_to_id[s] = state_counter
        decoded_label = DecodeLabel(s[1], n_prop)
        label[state_counter] = decoded_label
        # Don't cares on alive are problematic, let's hope we don't get them
        if decoded_label[0] == None:
            raise Exception("I don't know what to do with a state that is both alive and not alive! {0}".format(s))
        state_counter = state_counter + 1
    
    #print("New states:")
    #print(new_states)

    # Add transitions
    new_state_to_successors = {}
    successors = {}
    for s in new_states:
        new_state_to_successors[s] = []
        s_id = new_state_to_id[s]
        successors[s_id] = set()
        for succ in new_state_to_states.get(s, []):
            for succ_label in state_to_labels[succ]:
                new_state_to_successors[s].append((succ, succ_label))
                successors[s_id].add(new_state_to_id[(succ, succ_label)])

    # At this point the NFA is still enlarged (it still contains states with not alive)

    # Collect states that are not alive (first proposition is False)
    to_be_removed = set()
    for s_id in successors:
        if label[s_id][0] == False:
            to_be_removed.add(s_id)

    # Remove not alive states and the alive proposition altogether
    state_counter = 0
    old_id_to_new = {}
    for s_id in successors:
        if s_id in to_be_removed:
            continue                            
        old_id_to_new[s_id] = state_counter
        state_counter = state_counter + 1

    #print(old_id_to_new)
        
    new_successors = {}
    new_predecessors = {}
    new_label = {}
    new_accepting = set()
    new_initial = set()
    for s in new_states:
        s_id = new_state_to_id[s]
        if s_id in to_be_removed:
            continue                            
        new_s_id = old_id_to_new[s_id]
        if s[0].index == initial_state_id:
            new_initial.add(new_s_id)
        # Cut the first proposition (alive)
        new_label[new_s_id] = label[s_id][1:]
        if new_successors.get(new_s_id) == None:
            new_successors[new_s_id] = set()
        if new_predecessors.get(new_s_id) == None:
            new_predecessors[new_s_id] = set()
        for t_id in successors[s_id]:
            if t_id in to_be_removed:
                new_accepting.add(new_s_id)
                continue
            new_t_id = old_id_to_new[t_id]
            if new_predecessors.get(new_t_id) == None:
                new_predecessors[new_t_id] = set()
            new_successors[new_s_id].add(new_t_id)
            new_predecessors[new_t_id].add(new_s_id)

    AP_list = automaton.header.propositions[1:]
    return NFA(AP_list, new_label, new_successors, new_predecessors, frozenset(new_initial), frozenset(new_accepting))


def normalizeNFA(aut: NFA):
    """Ensures that initial (resp., accepting) states do not have incoming (resp., outgoing) edges"""
    n_states = len(aut.successors)
    new_id = n_states
    new_label = dict(aut.label)
    new_successors = dict(aut.successors)
    new_predecessors = dict(aut.predecessors)
    new_initial = set()
    new_accepting = set()
    for s in aut.initial:
        if len(aut.predecessors[s]) > 0:
            # initial_copy[s] = new_id
            new_label[new_id] = aut.label[s]
            new_initial.add(new_id)
            new_predecessors[new_id] = set()
            new_successors[new_id] = aut.successors[s]
            for t in new_successors[new_id]:
                new_predecessors[t].add(new_id)
            new_id = new_id + 1
        else:
            new_initial.add(s)
    for s in aut.accepting:
        if len(aut.successors[s]) > 0:
            # accepting_copy[s] = new_id
            new_label[new_id] = aut.label[s]
            new_accepting.add(new_id)
            new_predecessors[new_id] = aut.predecessors[s]
            new_successors[new_id] = set()
            for t in new_predecessors[new_id]:
                new_successors[t].add(new_id)
            new_id = new_id + 1
        else:
            new_accepting.add(s)
    return NFA(aut.AP, new_label, new_successors, new_predecessors, frozenset(new_initial), frozenset(new_accepting))



def anySetToString(set):
    return "{" + ", ".join(str(x) for x in set) + "}"
    

def automatonToC(aut: NFA, name: str):
    print("namespace Automata {\n   using TFD = Automaton::TFD;\n")
    print("   // Atomic props: {}".format(aut.AP))     
    print("   Automaton", name, "= {")
    # Predecessors
    print("      .Predecessor = {\n          ",end="")
    preds = []
    nstates = len(aut.predecessors)
    for state in range(0,nstates):
        preds.append(anySetToString(aut.predecessors[state]))
    print(",\n          ".join(preds))
    print("      },")
    # Initial states
    print("      .Initial =", anySetToString(aut.initial), ",")
    # Accepting states
    print("      .Final =", anySetToString(aut.accepting), ",")
    # Labels
    convertLabel = { None: "TFD::DontC", True: "TFD::True", False: "TFD::False" }
    print("      .Label = {\n          ", end="")
    labels = []
    for state in range(0,nstates):
        label = [ convertLabel[l] for l in aut.label[state] ]
        labels.append("{" + ", ".join(label) + "}")
    print(",\n          ".join(labels))
    print("      }")
    print("   };")
    print("}")

def automatonToDot(aut: NFA, name: str):
    print("digraph", name, "{")
    print(" rankdir=LR\n labelloc=\"\\t\"\n node [shape=\"ellipse\",width=\"0.5\",height=\"0.5\"]\n I [label=\"\", style=invis, width=0]")
    for s in aut.initial:
        print(" I -> {}".format(s))
    nstates = len(aut.predecessors)
    for s in range(0, nstates):
        if s in aut.accepting:
            print("{0} [label=\"{0} {1}\", peripheries=2]".format(s, aut.label[s]))
        else:
            print("{0} [label=\"{0} {1}\"]".format(s, aut.label[s]))
        for t in aut.successors[s]:
            print("{0} -> {1}".format(s,t))
    print("}")
    
def main():
    if len(sys.argv) > 1:
        name = sys.argv[1].replace(".","")
        f = open(sys.argv[1])
    else:
        name = "Aut"
        f = sys.stdin

    parser = HOAParser()
    buchi_automaton = parser(f.read())
    NFA = BuchiToNFA(buchi_automaton)
    # print(NFA)
    normalized_NFA = normalizeNFA(NFA)
    # print("Normalized: ", normalized_NFA)
    automatonToC(normalized_NFA, name)
    #automatonToDot(normalized_NFA, name)

main()
