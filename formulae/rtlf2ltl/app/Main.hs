
module Main (

  {- * The @main@ function -}
  main

) where

--------------------------------------------------------------------------------

import           Data.Set
import           Lib
import           System.Environment

--------------------------------------------------------------------------------

main
  :: IO ()
main =
  do {
    args <- getArgs;
    case args of
      [] ->
        error "#### Missing Property Argument ####"
      prp : lst ->
        do {
          putStr ("Property: " ++ prp ++ "\n");

          putStrLn "RTL-F 2 LTL Tool\n";

          putStr "Original RTLf Formula:\n";
          print (frmRTLF prp lst);
          putStrLn "";

          putStr "Atomic Propositions of the Polyhedral System:\n";
          print (addap `union` apset (frmRTLF prp lst));
          putStrLn "";

          putStr "Atomic Propositions of the RTLf Formula:\n";
          print (apset (frmRTLF prp lst));
          putStrLn "";

          putStr "Equivalent LTLf Formula:\n";
          print (frmLTLF prp lst);
          putStrLn "";

          putStr "Simplified LTLf Formula:\n";
          print (simLTLF prp lst);
          putStrLn "";

          -- putStr "Negation normal form LTLF Formula:\n";
          -- print nnfLTLF;
          -- putStrLn "";

          putStr "Equivalent LTL Formula:\n";
          print (frmLTL prp lst);
        }
  }
;

frmLTLF :: String -> [String] -> TL String
frmLTLF prp lst = rtlf2ltlf addap (frmRTLF prp lst)

simLTLF :: String -> [String] -> TL String
simLTLF prp lst = simplify (frmLTLF prp lst)

-- nnfLTLF :: String -> [String] -> TL String
-- nnfLTLF prp lst = nnf (frmLTLF prp lst)

frmLTL :: String -> [String] -> TL String
frmLTL prp lst = ltlf2ltl (simLTLF prp lst)

addap :: Set String
addap = empty

--------------------------------------------------------------------------------

tzero :: TL String
tzero = P "t=0" -- "t=0"

inv :: TL String
inv = G (P "a>=0 and b>=0") -- G "a >= 0 and b >= 0"

initinv :: TL String -> TL String
initinv frm = A tzero (A inv frm)

initnotinv :: TL String -> TL String
initnotinv frm = A tzero (A (N inv) frm)

--------------------------------------------------------------------------------

alt :: Int -> TL String -> TL String -> TL String
alt 0 frm1 _    = F frm1
alt 1 frm1 frm2 = F (A frm1 (F frm2))
alt k frm1 frm2 = F (A frm1 (F (A frm2 (alt (k - 2) frm1 frm2))))

--------------------------------------------------------------------------------

-- The system remains in the invarinat until at least time T
prp1 :: TL String
prp1 = initinv (F (P "t=T")) -- "t=0" AND INV AND F "t=T"

-- Is there a way to fail the invariant within T time
prp2 :: TL String
prp2 = initnotinv (G (P "t<=T")) -- "t=0" AND !INV AND G "t<=T"

-- K alternations between a>b and b>a (do the results coincide for all K?)
prp3 :: Int -> TL String
prp3 k = initinv (alt k (P "a>b") (P "b>a")) -- "t=0" AND INV AND "k alternations of "a>b" and "b>a"

-- K alternations between a>b+1 and b>a+1 within time T (this should differ for different K!)
prp4 :: Int -> TL String
prp4 k = initinv (A (G (P "t<=T")) (alt k (P "a>b+1") (P "b>a+1"))) -- "t=0" AND INV AND G "t <= T" AND "k alternations of "a>b+1" and "b>a+1"

-- K alternations between a>b and b>a (do the results coincide for all K?)
prp5 :: Int -> TL String
prp5 k = A (G (P "t<=T")) (prp3 k) -- G "t <= T" AND prp3 k

-- K alternations between p and q (do the results coincide for all K?)
prp6 :: Int -> TL String
prp6 k = A (G (P "inv")) (alt k (P "p") (P "q")) -- INV AND "k alternations of "p" and "q"

--------------------------------------------------------------------------------

frmRTLF :: String -> [String] -> TL String
frmRTLF prp args =
  case prp of
    "prp1" ->
      prp1
    "prp2" ->
      prp2
    "prp3" ->
      case args of
        [] ->
          error "#### Missing Index Argument ####"
        ind : _ ->
          prp3 (read ind)
    "prp4" ->
      case args of
        [] ->
          error "#### Missing Index Argument ####"
        ind : _ ->
          prp4 (read ind)
    "prp5" ->
      case args of
        [] ->
          error "#### Missing Index Argument ####"
        ind : _ ->
          prp5 (read ind)
    "prp6" ->
      case args of
        [] ->
          error "#### Missing Index Argument ####"
        ind : _ ->
          prp6 (read ind)
    _ ->
      error "#### Unknown Property ####"
