
{-# LANGUAGE FlexibleInstances #-}

module Lib (

  {- * Library types and functions -}

  TL(..),
  Alivable(..),
  Singleable(..),

  apset,
  rtlf2ltlf,
  rtlf2ltlfaxm,
  rtlf2ltlftrn,
  ltlf2ltl,
  ltlf2ltlaxm,
  ltlf2ltltrn,
  nnf,
  normalise,
  simplify,

  lastmoment,
  weakversion,
  weaknext,
  changeap,
  chgapnoworthen

) where

--------------------------------------------------------------------------------

import           Data.Set as Set

--------------------------------------------------------------------------------

data TL ap
  = Tt                  -- True
  | Ff                  -- False
  | P ap                -- Atomic Proposition of type `ap'
  | N (TL ap)           -- Negation
  | A (TL ap) (TL ap)   -- And
  | O (TL ap) (TL ap)   -- Or
  | E (TL ap) (TL ap)   -- Equal
  | D (TL ap) (TL ap)   -- Different (xor)
  | X (TL ap)           -- Next
  | F (TL ap)           -- Eventually
  | G (TL ap)           -- Globally
  | U (TL ap) (TL ap)   -- Until
  | R (TL ap) (TL ap)   -- Release
;

instance (Show ap) => Show (TL ap) where
  -- show :: Show ap => TL ap -> String
  show Tt            = "true";
  show Ff            = "false";
  show (P ap)        = show ap;
  show (N frm)       = "!" ++ show frm
  show (A frm1 frm2) = "(" ++ show frm1 ++ " & " ++ show frm2 ++ ")"
  show (O frm1 frm2) = "(" ++ show frm1 ++ " | " ++ show frm2 ++ ")"
  show (E frm1 frm2) = "(" ++ show frm1 ++ " <-> " ++ show frm2 ++ ")"
  show (D frm1 frm2) = "(" ++ show frm1 ++ " xor " ++ show frm2 ++ ")"
  show (X frm)       = "X " ++ show frm
  show (F frm)       = "F " ++ show frm
  show (G frm)       = "G " ++ show frm
  show (U frm1 frm2) = "(" ++ show frm1 ++ " U " ++ show frm2 ++ ")"
  show (R frm1 frm2) = "(" ++ show frm1 ++ " R " ++ show frm2 ++ ")"
;

--------------------------------------------------------------------------------

class Alivable typ where
  alive :: typ

instance Alivable Int where
  alive = 0

instance Alivable Char where
  alive = 'A'

instance Alivable String where
  alive = "alive"

instance Alivable ap => Alivable (TL ap) where
  alive = P alive

--------------------------------------------------------------------------------

class Singleable typ where
  sing :: typ

instance Singleable Int where
  sing = 1

instance Singleable Char where
  sing = 'S'

instance Singleable String where
  sing = "sing"

instance Singleable ap => Singleable (TL ap) where
  sing = P sing

--------------------------------------------------------------------------------

lastmoment :: TL ap
lastmoment = N (X Tt)

weakversion :: TL ap -> TL ap
weakversion = O lastmoment

weaknext :: TL ap -> TL ap
weaknext frm = weakversion (X frm)

changeap :: ap -> TL ap
changeap p = weakversion (E (P p) (X (N (P p))))

chgapnoworthen :: ap -> TL ap
chgapnoworthen p = O (changeap p) (X (changeap p))

--------------------------------------------------------------------------------

apset :: Ord ap => TL ap -> Set ap
apset (P ap)        = singleton ap
apset (N frm)       = apset frm
apset (A frm1 frm2) = apset frm1 `union` apset frm2
apset (O frm1 frm2) = apset frm1 `union` apset frm2
apset (E frm1 frm2) = apset frm1 `union` apset frm2
apset (D frm1 frm2) = apset frm1 `union` apset frm2
apset (X frm)       = apset frm
apset (F frm)       = apset frm
apset (G frm)       = apset frm
apset (U frm1 frm2) = apset frm1 `union` apset frm2
apset (R frm1 frm2) = apset frm1 `union` apset frm2
apset _             = empty

--------------------------------------------------------------------------------

rtlf2ltlf :: (Ord ap, Singleable ap) => Set ap -> TL ap -> TL ap
rtlf2ltlf aps frm =
  A
    (rtlf2ltlfaxm (aps `union` apset frm))
    (rtlf2ltlftrn frm)
;


rtlf2ltlfaxm :: Singleable ap => Set ap -> TL ap
rtlf2ltlfaxm set =
  A rtlf2ltlfaxm1 (A rtlf2ltlfaxm2 (A rtlf2ltlfaxm3 (rtlf2ltlfaxm4 set)))
;

rtlf2ltlfaxm1 :: Singleable ap => TL ap
rtlf2ltlfaxm1 = sing

rtlf2ltlfaxm2 :: Singleable ap => TL ap
rtlf2ltlfaxm2 = F (A sing lastmoment)

rtlf2ltlfaxm3 :: Singleable ap => TL ap
rtlf2ltlfaxm3 = G (changeap sing)

rtlf2ltlfaxm4 :: Set ap -> TL ap
rtlf2ltlfaxm4 _ = Tt -- weaknext (G (Set.foldr (O . chgapnoworthen) Ff aps))


rtlf2ltlftrn :: Singleable ap => TL ap -> TL ap

rtlf2ltlftrn (X _)         = error "##** Next operator not included in RTL **##"

rtlf2ltlftrn (N frm)       = N (rtlf2ltlftrn frm)
rtlf2ltlftrn (A frm1 frm2) = A (rtlf2ltlftrn frm1) (rtlf2ltlftrn frm2)
rtlf2ltlftrn (O frm1 frm2) = O (rtlf2ltlftrn frm1) (rtlf2ltlftrn frm2)
rtlf2ltlftrn (E frm1 frm2) = E (rtlf2ltlftrn frm1) (rtlf2ltlftrn frm2)
rtlf2ltlftrn (D frm1 frm2) = D (rtlf2ltlftrn frm1) (rtlf2ltlftrn frm2)

rtlf2ltlftrn (F frm)       = rtlf2ltlftrn (U Tt frm)
rtlf2ltlftrn (G frm)       = rtlf2ltlftrn (R Ff frm)
rtlf2ltlftrn (R frm1 frm2) = N (rtlf2ltlftrn (U (N frm1) (N frm2)))

rtlf2ltlftrn (U frm1 frm2) = O (A sing (X zeta)) (A (N sing) zeta)
  where zeta = U (rtlf2ltlftrn frm1) (rtlf2ltlftrn (A frm2 (O frm1 sing)))

rtlf2ltlftrn frm           = frm

--------------------------------------------------------------------------------

ltlf2ltl :: Alivable ap => TL ap -> TL ap
ltlf2ltl frm = A ltlf2ltlaxm (ltlf2ltltrn frm)

ltlf2ltlaxm :: Alivable ap => TL ap
ltlf2ltlaxm = A alive (U alive (G (N alive)))

ltlf2ltltrn :: Alivable ap => TL ap -> TL ap

ltlf2ltltrn (N frm)       = N (ltlf2ltltrn frm)

ltlf2ltltrn (A frm1 frm2) = A (ltlf2ltltrn frm1) (ltlf2ltltrn frm2)
ltlf2ltltrn (O frm1 frm2) = O (ltlf2ltltrn frm1) (ltlf2ltltrn frm2)
ltlf2ltltrn (E frm1 frm2) = E (ltlf2ltltrn frm1) (ltlf2ltltrn frm2)
ltlf2ltltrn (D frm1 frm2) = D (ltlf2ltltrn frm1) (ltlf2ltltrn frm2)

ltlf2ltltrn (X frm)       = X (A alive (ltlf2ltltrn frm))
ltlf2ltltrn (F frm)       = F (A alive (ltlf2ltltrn frm))
ltlf2ltltrn (G frm)       = G (O (N alive) (ltlf2ltltrn frm))
ltlf2ltltrn (U frm1 frm2) = U (ltlf2ltltrn frm1) (A alive (ltlf2ltltrn frm2))
ltlf2ltltrn (R frm1 frm2) = R (ltlf2ltltrn frm1) (O (N alive) (ltlf2ltltrn frm2))

ltlf2ltltrn frm           = frm

--------------------------------------------------------------------------------

nnf :: TL ap -> TL ap

nnf Tt                = Tt
nnf Ff                = Ff
nnf (A frm1 frm2)     = A (nnf frm1) (nnf frm2)
nnf (O frm1 frm2)     = O (nnf frm1) (nnf frm2)
nnf (E frm1 frm2)     = E (nnf frm1) (nnf frm2)
nnf (D frm1 frm2)     = D (nnf frm1) (nnf frm2)
nnf (X frm)           = X (nnf frm)
nnf (F frm)           = F (nnf frm)
nnf (G frm)           = G (nnf frm)
nnf (U frm1 frm2)     = U (nnf frm1) (nnf frm2)
nnf (R frm1 frm2)     = R (nnf frm1) (nnf frm2)

nnf (N Tt)            = Ff
nnf (N Ff)            = Tt
nnf (N (A frm1 frm2)) = O (nnf (N frm1)) (nnf (N frm2))
nnf (N (O frm1 frm2)) = A (nnf (N frm1)) (nnf (N frm2))
nnf (N (E frm1 frm2)) = D (nnf frm1) (nnf frm2)
nnf (N (D frm1 frm2)) = E (nnf frm1) (nnf frm2)
nnf (N (X Tt))        = N (X Tt)
nnf (N (X Ff))        = Tt
nnf (N (X frm))       = X (nnf (N frm)) -- weaknext (nnf (N frm))
nnf (N (F frm))       = G (nnf (N frm))
nnf (N (G frm))       = F (nnf (N frm))
nnf (N (U frm1 frm2)) = R (nnf (N frm1)) (nnf (N frm2))
nnf (N (R frm1 frm2)) = U (nnf (N frm1)) (nnf (N frm2))
nnf (N (N frm))       = nnf frm

nnf frm               = frm

--------------------------------------------------------------------------------

normalise :: TL ap -> TL ap  -- Complete this function!
normalise = id

--------------------------------------------------------------------------------

simplify :: TL ap -> TL ap

simplify (N frm) =
  case simplify frm of
    Ff ->
      Tt
    Tt ->
      Ff
    N frm' ->
      frm'
    E frm1 frm2 ->
      D frm1 frm2
    D frm1 frm2 ->
      E frm1 frm2
    frm' ->
      N frm'
;

simplify (A frm1 frm2) =
  let frm2' = simplify frm2 in
  case simplify frm1 of
    Ff ->
      Ff
    Tt ->
      frm2'
    frm1' ->
      case frm2' of
        Ff ->
          Ff
        Tt ->
          frm1'
        _ -> A frm1' frm2'
;

simplify (O frm1 frm2) =
  let frm2' = simplify frm2 in
  case simplify frm1 of
    Tt ->
      Tt
    Ff ->
      frm2'
    frm1' ->
      case frm2' of
        Tt ->
          Tt
        Ff ->
          frm1'
        _ -> O frm1' frm2'
;

simplify (E frm1 frm2) =
  let frm2' = simplify frm2 in
  case simplify frm1 of
    Tt ->
      frm2'
    Ff ->
      simplify (N frm2')
    frm1' ->
      case frm2' of
        Tt ->
          frm1'
        Ff ->
          simplify (N frm1')
        _ -> E frm1' frm2'
;

simplify (D frm1 frm2) =
  let frm2' = simplify frm2 in
  case simplify frm1 of
    Ff ->
      frm2'
    Tt ->
      simplify (N frm2')
    frm1' ->
      case frm2' of
        Ff ->
          frm1'
        Tt ->
          simplify (N frm1')
        _ -> D frm1' frm2'
;

simplify (X frm) =
  X (simplify frm)
;

simplify (F frm) =
  F (simplify frm)
;

simplify (G frm) =
  G (simplify frm)
;

simplify (U frm1 frm2) =
  let frm2' = simplify frm2 in
  case simplify frm1 of
    Tt ->
      F frm2'
    frm1' ->
      U frm1' frm2'
;

simplify (R frm1 frm2) =
  let frm2' = simplify frm2 in
  case simplify frm1 of
    Ff ->
      G frm2'
    frm1' ->
      R frm1' frm2'
;

simplify frm = frm

--------------------------------------------------------------------------------
