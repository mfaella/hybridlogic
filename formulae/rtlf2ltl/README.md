
1) Install ghcup

2) (Optional) Install GHC 9.2.7 via ghcup

3) Modify formula 'frmRTLF' in app/Main.hs

4) Run "stack build" in project home

5) Run "stack run" in project home

6) Run "stack clean" in project home to clean the mess
