
#include <chrono>

#include "basics.h"
#include "loop.h"
#include "reach.hpp"
#include "ltlf_mc.hpp"

#include "automata/prp1.h"

#include "automata/prp2.h"

Automaton * Aut3[10];

#include "automata/prp31.h"
#include "automata/prp32.h"
#include "automata/prp33.h"
#include "automata/prp34.h"
#include "automata/prp35.h"
#include "automata/prp36.h"
#include "automata/prp37.h"
#include "automata/prp38.h"
#include "automata/prp39.h"
#include "automata/prp310.h"

Automaton * Aut4[10];

#include "automata/prp41.h"
#include "automata/prp42.h"
#include "automata/prp43.h"
#include "automata/prp44.h"
#include "automata/prp45.h"
#include "automata/prp46.h"
#include "automata/prp47.h"
#include "automata/prp48.h"
#include "automata/prp49.h"
#include "automata/prp410.h"

Automaton * Aut5[10];

#include "automata/prp51.h"
#include "automata/prp52.h"
#include "automata/prp53.h"
#include "automata/prp54.h"
#include "automata/prp55.h"
#include "automata/prp56.h"
#include "automata/prp57.h"
#include "automata/prp58.h"
#include "automata/prp59.h"
#include "automata/prp510.h"

Automaton * Aut6[11];

#include "automata/prp61.h"
#include "automata/prp62.h"
#include "automata/prp63.h"
#include "automata/prp64.h"
#include "automata/prp65.h"
#include "automata/prp66.h"
#include "automata/prp67.h"
#include "automata/prp68.h"
#include "automata/prp69.h"
#include "automata/prp610.h"
#include "automata/prp611.h"

using namespace std;
using namespace std::chrono;

PPS example_water_tanks1(Automaton & aut);
PPS example_water_tanks2(Automaton & aut);
PPS example_water_tanks3E(Automaton & aut);
PPS example_water_tanks3O(Automaton & aut);
PPS example_water_tanks4E(Automaton & aut);
PPS example_water_tanks4O(Automaton & aut);
PPS example_water_tanks5E(Automaton & aut);
PPS example_water_tanks5O(Automaton & aut);
PPS example_VE(Automaton & aut);
PPS example_VO(Automaton & aut);

int main(int argc, char * argv[]) {

  Aut3[0] = &Automata::Aut31;
  Aut3[1] = &Automata::Aut32;
  Aut3[2] = &Automata::Aut33;
  Aut3[3] = &Automata::Aut34;
  Aut3[4] = &Automata::Aut35;
  Aut3[5] = &Automata::Aut36;
  Aut3[6] = &Automata::Aut37;
  Aut3[7] = &Automata::Aut38;
  Aut3[8] = &Automata::Aut39;
  Aut3[9] = &Automata::Aut310;

  Aut4[0] = &Automata::Aut41;
  Aut4[1] = &Automata::Aut42;
  Aut4[2] = &Automata::Aut43;
  Aut4[3] = &Automata::Aut44;
  Aut4[4] = &Automata::Aut45;
  Aut4[5] = &Automata::Aut46;
  Aut4[6] = &Automata::Aut47;
  Aut4[7] = &Automata::Aut48;
  Aut4[8] = &Automata::Aut49;
  Aut4[9] = &Automata::Aut410;

  Aut5[0] = &Automata::Aut51;
  Aut5[1] = &Automata::Aut52;
  Aut5[2] = &Automata::Aut53;
  Aut5[3] = &Automata::Aut54;
  Aut5[4] = &Automata::Aut55;
  Aut5[5] = &Automata::Aut56;
  Aut5[6] = &Automata::Aut57;
  Aut5[7] = &Automata::Aut58;
  Aut5[8] = &Automata::Aut59;
  Aut5[9] = &Automata::Aut510;

  Aut6[0] = &Automata::Aut61;
  Aut6[1] = &Automata::Aut62;
  Aut6[2] = &Automata::Aut63;
  Aut6[3] = &Automata::Aut64;
  Aut6[4] = &Automata::Aut65;
  Aut6[5] = &Automata::Aut66;
  Aut6[6] = &Automata::Aut67;
  Aut6[7] = &Automata::Aut68;
  Aut6[8] = &Automata::Aut69;
  Aut6[9] = &Automata::Aut610;
  Aut6[10] = &Automata::Aut611;

  if (argc > 1) {
    auto beg = high_resolution_clock::now();

    if (strcmp(argv[1], "1") == 0) {
      cout << example_water_tanks1(Automata::Aut1) << endl;
    }

    if (strcmp(argv[1], "2") == 0) {
      cout << example_water_tanks2(Automata::Aut2) << endl;
    }

    if (strcmp(argv[1], "3") == 0) {
      int i = atoi(argv[2]) - 1;
      if (i < 10) {
        if (i % 2 == 1) {
          cout << example_water_tanks3E(*Aut3[i]) << endl;
        } else {
          cout << example_water_tanks3O(*Aut3[i]) << endl;
        }
      }
    }

    if (strcmp(argv[1], "4") == 0) {
      int i = atoi(argv[2]) - 1;
      if (i < 10) {
        if (i % 2 == 1) {
          cout << example_water_tanks4E(*Aut4[i]) << endl;
        } else {
          cout << example_water_tanks4O(*Aut4[i]) << endl;
        }
      }
    }

    if (strcmp(argv[1], "5") == 0) {
      int i = atoi(argv[2]) - 1;
      if (i < 10) {
        if (i % 2 == 1) {
          cout << example_water_tanks5E(*Aut5[i]) << endl;
        } else {
          cout << example_water_tanks5O(*Aut5[i]) << endl;
        }
      }
    }

    if (strcmp(argv[1], "6") == 0) {
      int i = atoi(argv[2]) - 1;
      if (i < 11) {
        if (i % 2 == 1) {
          cout << example_VE(*Aut6[i]) << endl;
        } else {
          cout << example_VO(*Aut6[i]) << endl;
        }
      }
    }

    auto end = high_resolution_clock::now();
    auto durmicsec = duration_cast<microseconds>(end - beg);
    auto durmilsec = duration_cast<milliseconds>(end - beg);
    auto dursec = duration_cast<seconds>(end - beg);
    cout << "Elapsed Time (microseconds): " << durmicsec.count() << endl;
    cout << "Elapsed Time (milliseconds): " << durmilsec.count() << endl;
    cout << "Elapsed Time (seconds): " << dursec.count() << endl;
  }

}

PPS example_water_tanks1(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), tT(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  tT.add_disjunct(make_poly({ t == 10 }, DIM));         // t = T

  int sing = 1;
  State2PPS polysys = { tT, DUMMY, inv, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks2(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), tT(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  tT.add_disjunct(make_poly({ t <= 10 }, DIM));         // t <= T

  int sing = 1;
  State2PPS polysys = { inv, DUMMY, tT, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks3O(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), aGb(DIM, EMPTY), bGa(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  aGb.add_disjunct(make_poly({ a > b }, DIM));          // a > b
  bGa.add_disjunct(make_poly({ b > a }, DIM));          // b > a

  int sing = 1;
  State2PPS polysys = { bGa, DUMMY, aGb, inv, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks3E(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), aGb(DIM, EMPTY), bGa(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  aGb.add_disjunct(make_poly({ a > b }, DIM));          // a > b
  bGa.add_disjunct(make_poly({ b > a }, DIM));          // b > a

  int sing = 1;
  State2PPS polysys = { aGb, DUMMY, bGa, inv, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks4O(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), tT(DIM, EMPTY), aGb1(DIM, EMPTY), bGa1(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  tT.add_disjunct(make_poly({ t <= 10 }, DIM));         // t <= T
  aGb1.add_disjunct(make_poly({ a > b + 1 }, DIM));     // a >= b + 1
  bGa1.add_disjunct(make_poly({ b > a + 1 }, DIM));     // b >= a + 1

  int sing = 1;
  State2PPS polysys = { bGa1, DUMMY, aGb1, inv, tT, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks4E(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), tT(DIM, EMPTY), aGb1(DIM, EMPTY), bGa1(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  tT.add_disjunct(make_poly({ t <= 10 }, DIM));         // t <= T
  aGb1.add_disjunct(make_poly({ a > b + 1 }, DIM));     // a >= b + 1
  bGa1.add_disjunct(make_poly({ b > a + 1 }, DIM));     // b >= a + 1

  int sing = 1;
  State2PPS polysys = { aGb1, DUMMY, bGa1, inv, tT, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks5O(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), tT(DIM, EMPTY), aGb(DIM, EMPTY), bGa(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  tT.add_disjunct(make_poly({ t <= 10 }, DIM));         // t <= T
  aGb.add_disjunct(make_poly({ a > b }, DIM));          // a > b
  bGa.add_disjunct(make_poly({ b > a }, DIM));          // b > a

  int sing = 1;
  State2PPS polysys = { bGa, DUMMY, aGb, tT, inv, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_water_tanks5E(Automaton & aut) {
  /* Define the polyhedral system
     Variables a, b, t
     Flow = see paper
   */
  Variable a(0), b(1), t(2);
  const int DIM = 3;

  Poly flow = make_poly({ a >= -1, a <= 1, b >= -2, b <= 2, a + b >= -2, a + b <= 2, t == 1 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), inv(DIM, EMPTY), t0(DIM, EMPTY), tT(DIM, EMPTY), aGb(DIM, EMPTY), bGa(DIM, EMPTY);
  inv.add_disjunct(make_poly({ a >= 0, b >= 0 }, DIM)); // INV
  t0.add_disjunct(make_poly({ t == 0 }, DIM));          // t = 0
  tT.add_disjunct(make_poly({ t <= 10 }, DIM));         // t <= T
  aGb.add_disjunct(make_poly({ a > b }, DIM));          // a > b
  bGa.add_disjunct(make_poly({ b > a }, DIM));          // b > a

  int sing = 1;
  State2PPS polysys = { aGb, DUMMY, bGa, tT, inv, t0 };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_VE(Automaton & aut) {
  /* Two diagonal segments (<-shaped). */
  Variable a(0), b(1);
  const int DIM = 2;

  // Poly flow = make_poly({ a == 1, b >= -2, b <= 2 });
  // Poly flow = make_poly({ a == 1, b >= -5, b <= 5 });
  Poly flow = make_poly({ a == 1, b >= -4, b <= 4 });
  // Poly flow = make_poly({ a == 1, 2 * b >= -7, 2 * b <= 7 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), p(DIM, EMPTY), q(DIM, EMPTY), inv(DIM, EMPTY);
  p.add_disjunct(make_poly({ a >= 0, a <= 21, a == b }, DIM)); // p
  q.add_disjunct(make_poly({ a >= 0, a <= 21, a == -b }, DIM)); // q
  inv.add_disjunct(make_poly({ a <= 21, a >= b, -a <= b }, DIM)); // INV

  int sing = 1;
  State2PPS polysys = { p, DUMMY, q, inv };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}

PPS example_VO(Automaton & aut) {
  /* Two diagonal segments (<-shaped). */
  Variable a(0), b(1);
  const int DIM = 2;

  // Poly flow = make_poly({ a == 1, b >= -2, b <= 2 });
  // Poly flow = make_poly({ a == 1, b >= -5, b <= 5 });
  Poly flow = make_poly({ a == 1, b >= -4, b <= 4 });
  // Poly flow = make_poly({ a == 1, 2 * b >= -7, 2 * b <= 7 });
  Poly reverse_flow(flow);
  prepoly_assign(reverse_flow);

  PPS DUMMY(DIM, EMPTY), p(DIM, EMPTY), q(DIM, EMPTY), inv(DIM, EMPTY);
  p.add_disjunct(make_poly({ a >= 0, a <= 21, a == b }, DIM)); // p
  q.add_disjunct(make_poly({ a >= 0, a <= 21, a == -b }, DIM)); // q
  inv.add_disjunct(make_poly({ a <= 21, a >= b, -a <= b }, DIM)); // INV

  int sing = 1;
  State2PPS polysys = { q, DUMMY, p, inv };

  return ltlf_mc(aut, { .dimension = DIM, .preflow = reverse_flow, .AP2PPS = polysys }, sing);
}
