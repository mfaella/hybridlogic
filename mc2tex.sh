./fossacs $1 $2 | tail -1 > ./tex/"$1$2".out
./remove_commas.sh ./tex/"$1$2".out > ./tex/"$1$2".poly

if [ $1 == "1" ]
then
  ./poly2tex -crop 0 10 -10 10 -f ./tex/"$1$2".poly A B > ./tex/"$1$2".tex
fi

if [ $1 == "2" ]
then
  ./poly2tex -crop 0 10 -10 10 -f ./tex/"$1$2".poly A B > ./tex/"$1$2".tex
fi

if [ $1 == "3" ]
then
  ./poly2tex -crop 0 10 -10 10 -f ./tex/"$1$2".poly A B > ./tex/"$1$2".tex
fi

if [ $1 == "4" ]
then
  ./poly2tex -crop 0 50 -50 50 -f ./tex/"$1$2".poly A B > ./tex/"$1$2".tex
fi

if [ $1 == "5" ]
then
  ./poly2tex -crop 0 50 -10 50 -f ./tex/"$1$2".poly A B > ./tex/"$1$2".tex
fi

if [ $1 == "6" ]
then
  ./poly2tex -crop 0 13 -13 13 -f ./tex/"$1$2".poly A B > ./tex/"$1$2".tex
fi
